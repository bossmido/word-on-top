var express = require('express');
var connect = require('connect');
var form = require('connect-form');
var fs = require('fs');

var util = require('util');
var GridFS = require('GridFS').GridFS;
var mongoose = require('mongoose');
var MongoDB = require("mongodb");
var Step = require('step');
var util= require('util');

var db = new MongoDB.Db('upload', new MongoDB.Server('localhost', 27017, {}), {});
db.open(function(error, db) {
    if(error) {
        console.log(red + "Impossible de se connecter à MongoDB: " + reset);
    }
});


var app = express.createServer(form({
    keepExtensions : true
}));


var red, blue, reset;
red = '\033[31m';
blue = '\033[34m';
reset = '\033[0m';

function log(str) {
    console.log(red + str + reset);
}

function clone(srcInstance)
{
    /*Si l'instance source n'est pas un objet ou qu'elle ne vaut rien c'est une feuille donc on la retourne*/
    if(typeof(srcInstance) != 'object' || srcInstance == null)
    {
        return srcInstance;
    }
    /*On appel le constructeur de l'instance source pour crée une nouvelle instance de la même classe*/
    var newInstance = srcInstance.constructor();
    /*On parcourt les propriétés de l'objet et on les recopies dans la nouvelle instance*/
    for(var i in srcInstance)
    {
        if( newInstance[i])
            newInstance[i] = clone(srcInstance[i]);
    }
    /*On retourne la nouvelle instance*/
    return newInstance;
}

// switch between development and

// NODE_ENV=development node app.js
// OR
// NODE_ENV=production node app.js

// this always executes, no matter of production or dev environment
app.configure(function() {
    // this logs in a Apache style
    // app.use(express.logger());
    app.use(express.bodyParser({ uploadDir: __dirname + '/public/images' }));
    // this middleware will override our method
    // with what we passed into the hidden variable _method
    app.use(express.methodOverride());
    // methodOverride must be after the bodyParser
    app.use(express.static(__dirname + '/public'));
    app.use(express.errorHandler({
        dumpExceptions : true,
        showStack : true
    }));


});

app.configure('development', function() {
    app.use(express.logger());
    // this is the error handler, uncomment #1 to see it in action
    app.use(express.errorHandler({
        dumpExceptions : true,
        showStack : true
    }))
});

app.configure('production', function() {
    // this is the error handler for the production env
    app.use(express.errorHandler({
        dumpExceptions : false,
        showStack : false
    }));
});

app.set('views', __dirname + '/views');
app.set('view engine', 'jade')
// this is by default ->
// app.set('view options', {layout: true});
// if you don't want to have a layout though:
// app.set('view options', {layout: false});


app.get('/', function(req, res) {

    // #1
    // throw new Error('this is just my custom error');
    // res.send('some text');
    res.render('root');
});

app.post('/photos', function(req, res) {
    req.form.complete(function(err, fields, files) {
        log('test d\'erreur!');
        console.log(err);
        if(err) {
            next(err);
        } else {

            // db.collection('filelist', function(err, collection) {
            //     collection.insert({
            //         'filename' : files.image.path
            //     }, function() {
            //     });
            // });
    console.log(files.image);
    ins = fs.createReadStream(files.image.path);

    var buff = new Buffer(ins);

    ous = fs.createWriteStream(__dirname + '/public/images/' + files.image.filename);
    ous.write(buff);

    myFS.put(buff, files.image.filename, 'w', function(err) {
        console.log(red + "test de mongodb" + reset);
        console.log(red + err + reset);
    });
    util.pump(ins, ous, function(err) {
        if(err) {
            res.redirect('/photos');
            next(err);
        } else {
            res.redirect('/photos');
            console.log('\nUploaded %s to %s', files.image.filename, files.image.path);
                    // res.send('Uploaded ' + files.image.filename + ' to ' + files.image.path);
                }
            });
}
});
});

app.get('/photos', function(req, res) {
   // var fichiers = null;
   self  = this;
    //à remplacer par du code mongodb
    // db.collection('filelist', function(err, collection) {
    // collection.find('', function(err, cursor) {
    // if(err) {
    // console.log(red + "Probleme de recherche de fichier " + reset);
    // }
    // log(cursor + '');
    // cursor.each(function(err, doc) {
    // if(doc != null) {
    // console.log(blue+doc.filename+reset);
    // var file = myFS.get(doc.filename,function(err,data){
    // log(data);
    // });
    // files.push(file);
    // }
    // });
    // });
    //
    // });

var files = null;
Step(
    files = fs.readdirSync(__dirname + '/public/images/'),
    log('test de files'),
    console.log(red+self.fichiers+reset),

    res.render('photos', {
        'files' : files
    })
    );
});

app.get('/photos2', function(req, res) {
   // var fichiers = null;
   self  = this;
   var files = null;
   Step(
    files = fs.readdirSync(__dirname + '/public/images/'),
    log('test de files'),
    console.log(red+self.fichiers+reset),

    res.render('mockup_api_part2', {
        'files' : files
    })
    );
});

app.post('/ajax', function(req, res) {
    console.log(red+'POST'+reset);
    var json = req.body;
    //util.log(util.inspect(json));
    self = this;
    console.log(blue+"debut :"+reset);
    console.log(util.inspect(req.body.captions, true, null));
    db.collection('wordontop', function(err, collection) {
       var wordontop = collection;
    //   collection.reIndex();
    collection.findOne({'src' : req.body.src},function(err,c){
     if(c){
       if(!err){
        for(c3 in req.body.captions){
            if(c3){
                var count_duplicata = [];
                for(c4 in req.body.captions){
                    if (req.body.captions[c3].nbcaption == req.body.captions[c4].nbcaption && c3 != c4 ) {
                        count_duplicata.push(c4);
                    };
                }

                for(con in count_duplicata){
                    console.log("con");
                    req.body.captions.splice(count_duplicata[con],1);
                }
                console.log(blue+"apres elminitation doublons :"+reset);
                console.log(util.inspect(req.body, true, null));
            }
        }
        var j =0;
        for(c2 in req.body.captions){
            if (req.body.captions[c2]) {
                var isalready = false;
                for(c3 in c.captions){
                    if(req.body.captions[c2].nbcaption == c.captions[c3].nbcaption ){
                        isalready = true;
                        req.body.captions[c2].change = false;

                        c.captions[c3]  = req.body.captions[c2];
                    }
                }
                if(!isalready){
                    c.captions.push(req.body.captions[c2]);
                }
            }

            j++;
        }
        console.log(blue+"fin :"+reset);
        console.log(util.inspect(c, true, null));
        collection.save(c);

    }
}else{
   wordontop.save(json);
}

});



});


    // db.open(function(error, client) {
    //     client.collection('wordontop',function(err, collection) {
    //         collection.insert({'a' : 1});

    // });encodeURIComponent
    // });
});

app.get('/ajax/:src', function(req, res) {
    var json;
    console.log(red+"get ajax"+reset);
    if(db.collection('image').findOne({'title' : req.params.src})){
        db.collection('caption').find({'src' : req.params.src},function(err,cursor){
           cursor.distinct('_id',function(err,d){
            json.push(d);
        });
       });
        res.json(json);
    }
    

}
);



app.post('/ajax2/', function(req, res) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "X-Requested-With");  

   var self  = this.
   console.log(red+'POST'+reset);
   var json = req.body;
   self = this;
   if(!this.caption){

    this.caption =  db.collection('caption');
    
}

if(!this.image){

   this.image =  db.collection('image');
   this.image.ensureIndex({
    'title' : 1
},{unique : true,dropDups : true});
}

if('title' in json){
    console.log(red+'IMAGE'+reset);
    console.log(util.inspect(json));
    this.image.save(json);
    if('saveall' in json){
        for(c in json.captions){
            this.caption.save(c);
        }
        delete json.saveall;

    }else{
        delete this.captions;
    }
}else{
   console.log(red+'CAPTION'+reset);
    console.log(util.inspect(json));
   var objId = null;
   if(json &&( json['_id'] != '') && json['_id'] != null) {
    console.log(json['_id']);
    var objId = db.bson_serializer.ObjectID.createFromHexString(json['_id']);
    console.log(util.inspect(objId));
    delete json['_id'];
    json['_id'] =objId;
}else if(json['_id'] == ''){
    delete json['_id'];
}
this.caption.save(json,function(err,doc){
    if(doc && doc['_id']){
        console.log(util.inspect(doc));
        self.id_ajax2 = doc['_id'];

        console.log(util.inspect(doc['_id'].toHexString()));
    }

});



console.log(util.inspect(self.id_ajax2));
var res_tmp = null;
if(self.id_ajax2 != null) res_tmp  = self.id_ajax2.toHexString();
console.log(util.inspect(res_tmp));
res.json({'id' : res_tmp});
}
});



app.get('/ajax2/:src', function(req, res) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "X-Requested-With");  

   console.log(red+"get ajax"+reset);
   this.json_res = new Object();
   this.json_res.captions = new Array();
   var self = this;
   console.log(util.inspect(req.params.src));
   db.collection('caption').find({src : req.params.src},function(err,cursor){
    if(!err){
        cursor.toArray(function(err, docs) {
         console.log(util.inspect(docs));
         self.json_res.captions = docs;
         res.json(this.json_res);
     });

    }
});



});

app.delete('/ajax2/', function(req, res){
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "X-Requested-With");  

   console.log(red+"delete ajax"+reset);
   if(!this.caption){

    this.caption =  db.collection('caption');
    this.caption.ensureIndex({
        'src' : 1
    });
}

this.caption.remove(req.body.src);

});

app.listen(4000, "0.0.0.0");

console.log('Server listening on port 4000');
