$(document).ready(function($) {
// run the plugin
   (function() {
     var jasmineEnv = jasmine.getEnv();
     jasmineEnv.updateInterval = 1000;
     var htmlReporter = new jasmine.HtmlReporter();
     jasmineEnv.addReporter(htmlReporter);
     jasmineEnv.specFilter = function(spec) {
       return htmlReporter.specFilter(spec);
     };
     var currentWindowOnload = window.onload;
     window.onload = function() {
       if (currentWindowOnload) {
         currentWindowOnload();
       }
       execJasmine();
     };
     function execJasmine() {
       jasmineEnv.execute();
     }
   })();


  var event = $.Event('click');
  event.clientX = 1000;
  event.clientY = 1000;

  var foo ={};
  describe("wordontop", function() {

    it("load", function () {
      runs(function () {$('img:first').trigger(event);});
    });

    it("create a caption when click on it", function () {
      runs(function () {
        console.log('ooooooooooooooooooooooooooo');
      //  console.log(foo.GET('/ajax2/'));
      runs(function () {$('img:first').trigger('click');});
    });
    });


    it("check ddslick is loading", function () {
      runs(function () {

        expect($.fn.ddslick).not.toBeNull();
      });
    });
    it("check fontSelector is loading", function () {
      runs(function () {

        expect($.fn.fontSelector).not.toBeNull();
      });
    });
    it("check plugin for drag'n'drop  is loading", function () {
      runs(function () {

        expect($.ui).not.toBeNull();
      });

    });
  });

/*
  test les plus simples sur le changeemnt de styles
  */
  describe("style of the text", function() {
    describe("3D input", function() {

     $('.wordontopclass').click();
     it("should be able to apply 3D effect to the textarea", function () {
       waitsFor(function() {
        return WORDONTOP_FINISH_LOADING;
      }, "waiting plugin to load", 6000);
       runs(function () {
         $('input[name=mode_3d]').attr('checked',true);
         $('input[name=mode_3d]').trigger('change_3d');
         console.log($('#caption0').css('text-shadow'));
         expect($('#caption0').css('text-shadow')).not.toEqual(undefined);
       });


     });
   });
    describe("underline input", function() {
      it("should be able to underline the text", function () {
       waitsFor(function() {
        return WORDONTOP_FINISH_LOADING;
      }, "waiting plugin to load", 6000);
       runs(function () {
        $('input[name=mode_underline]').attr('checked',true);
        $('input[name=mode_underline]').trigger('change_underline');
        expect($('#caption0').css('text-decoration')).toEqual("underline");
      });

     });
    });
    describe("bold input", function() {
     it("should be able to make the text bolder", function () {
       waitsFor(function() {
        return WORDONTOP_FINISH_LOADING;
      }, "waiting plugin to load", 6000);
       runs(function () {
         $('input[name=mode_bold]').attr('checked',true);
         $('input[name=mode_bold]').trigger('change_bold');

         expect($('#caption0').css('font-weight')).toEqual("900");
       });
     });
   });
  });
/* test cas par cas de style*/
describe("submit of the caption", function() {
 it("should be able to save data on the server", function () {

   waitsFor(function() {
    return WORDONTOP_FINISH_LOADING;
  }, "waiting plugin to load", 6000);
   runs(function () {
    $('#caption0').val('test');
    $('#caption0').trigger('simulate_change');});
   runs(function () {
     $('.submit_caption').trigger('click');
   });
   if(WORDONTOP_LOADING_ERROR){
    expect(1).toEqual(2);
  }else{
   expect(1).toEqual(1);
 }
});

});
describe("wordontop", function() {
 it("could just fetch caption the server", function() {

   spyOn(wordontop_jQuery_less, 'GET').andCallThrough();
   wordontop_jQuery_less.GET('/ajax2/');
   expect(wordontop_jQuery_less.GET).toHaveBeenCalled();
 });

});
});

//after the test scroll to the result

setTimeout(function(){document.getElementById('HTMLReporter').scrollIntoView();},3000);


/*describe("Player", function() {
  var player;
  var song;

  beforeEach(function() {
    player = new Player();
    song = new Song();
  });

  it("should be able to play a Song", function() {
    player.play(song);
    expect(player.currentlyPlayingSong).toEqual(song);

    //demonstrates use of custom matcher
    expect(player).toBePlaying(song);
  });

  describe("when song has been paused", function() {
    beforeEach(function() {
      player.play(song);
      player.pause();
    });

    it("should indicate that the song is currently paused", function() {
      expect(player.isPlaying).toBeFalsy();

      // demonstrates use of 'not' with a custom matcher
      expect(player).not.toBePlaying(song);
    });

    it("should be possible to resume", function() {
      player.resume();
      expect(player.isPlaying).toBeTruthy();
      expect(player.currentlyPDlayingSong).toEqual(song);
    });
  });

  // demonstrates use of spies to intercept and test method calls
  it("tells the current song if the user has made it a favorite", function() {
    spyOn(song, 'persistFavoriteStatus');

    player.play(song);
    player.makeFavorite();

    expect(song.persistFavoriteStatus).toHaveBeenCalledWith(true);
  });

  //demonstrates use of expected exceptions
  describe("#resume", function() {
    it("should throw an exception if song is already playing", function() {
      player.play(song);

      expect(function() {
        player.resume();
      }).toThrow("song is already playing");
    });
  });
});*/