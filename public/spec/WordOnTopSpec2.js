$(document).ready(function($) {
  (function() {
     var jasmineEnv = jasmine.getEnv();
     jasmineEnv.updateInterval = 1000;
     var htmlReporter = new jasmine.HtmlReporter();
     jasmineEnv.addReporter(htmlReporter);
     jasmineEnv.specFilter = function(spec) {
       return htmlReporter.specFilter(spec);
     };
     var currentWindowOnload = window.onload;
     window.onload = function() {
       if (currentWindowOnload) {
         currentWindowOnload();
       }
       execJasmine();
     };
     function execJasmine() {
       jasmineEnv.execute();
     }
   })();
   
  var event = $.Event('click');
  event.clientX = 1000;
  event.clientY = 1000;

  describe("wordontop", function() {

    it("load", function () {
      runs(function () {$('img:first').trigger(event);});
    });

    it("should keep the old value of the caption", function () {
       waitsFor(function() {
      return $('#caption0').val() == 'test';
    }, "Spreadsheet calculation never completed", 3000);

      runs(function () {

      //  console.log(foo.GET('/ajax2/'));
    });
    });
  });
});
