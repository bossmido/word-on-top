/*
*   list :
*         -PixelEffect
*         -Paint
*         -Kaleidoscope
*         -Blur
          -Cloud
          -Stamp
          */




/*MOD
effect of happy running wild inocious pixel on the image ravaging all notion of beauty
*/
WordOnTopModPixelEffect = function(settings){if (settings.$canvas[0].getContext){
  var num = 56;
    var s = 2; //y "speed"
    var w = 99; //r_rect "size"
    var h = settings.height/num; 
    
    ranCol = '#' + ('00000' + (Math.random() * 16777216 << 0).toString(16)).substr(-6);
    x = 0;  //x,y start pos.
    y = 0;  
    WIDTH = (typeof window.innerWidth != 'undefined' ? window.innerWidth : document.body.offsetWidth);
    HEIGHT = 250; 

    function s_rect(x,y,w,h) {
      ctx.beginPath();
      ctx.rect(x,y,w,h);
      ctx.closePath();
      ctx.fill();
    }

    function init() {

      ctx = settings.$canvas[0].getContext("2d");
      wordontop_drawing = setInterval(draw, 10);
    }
    
    function erase(){
     ctx.globalCompositeOperation = 'source-out';
     ranCol = 'rgba(0,0,0,0.1)';
     for (var i = WIDTH - w; i >= 0; i--) {
       ctx.fillStyle = ranCol;
       w = (Math.random()*520)+50;
       s = (Math.random()*8)+8;

       s_rect(x-w, y+(i*h), w, h);
     }
     clearInterval(wordontop_drawing);
   }


   function draw() {
    if(!wordontop_finish_loading){
      for (i=0; i<num; i++) {
            y = 0;  //y start
            ranCol = '#' + ('00000' + (Math.random() * 16777216 << 0).toString(16)).substr(-6);  //flashing
            w = (Math.random()*520)+50;
            s = (Math.random()*8)+8;
            ctx.fillStyle = ranCol;
            s_rect(x, y+(i*h), w, h);
          }

          if (x + s > WIDTH - w || x + s < 0)
            s = -s;
          x += s;

        }else{
         erase();
       }}

       init();
     }else{
      alert("marche pas");
    }}


/*MOD
try to draw a spiral
*/
WordOnTopModCenterImage = function(settings){



  if (settings.$canvas[0].getContext){

    var Spiral = function(a) {
      this.initialize(a);
    }

    Spiral.prototype = {
      _a: 0.5,

      constructor: Spiral,

      initialize: function( a ) {
       if (a != null) this._a = a;
     },


     /* specify the increment in radians */
     points: function( rotations, increment ) {
       var maxAngle = Math.PI * 2 * rotations;
       var points = new Array();

       for (var angle = 0; angle <= maxAngle; angle = angle + increment)
       {
        points.push( this._point( angle ) );
      }

      return points;
    },

    _point: function( t ) {
      var x = this._a * t * Math.cos(t);
      var y = this._a * t * Math.sin(t);
      return { X: x, Y: y };
    }
  }


  var spiral = new Spiral(0.3);
  var points = spiral.points( 2, 0.01 );

                     var x = 0;  //x,y start pos.
                     var y = 0;  
                     var width_rectangle = 3;
                     var height_rectangle = 3;
                     var WIDTH = settings.width;
                     var HEIGHT = settings.height; 

                     function s_rect(x,y,w,h) {
                      ctx.beginPath();
                      ctx.rect(x,y,w,h);
                      ctx.closePath();
                      ctx.fill();
                    }

                    function init() {

                      ctx = settings.$canvas[0].getContext("2d");
                      wordontop_drawing = setInterval(draw, 10);
                    }


                    function roundedRect(ctx,x,y,width,height,radius){
                      ctx.beginPath();
                      ctx.moveTo(x,y+radius);
                      ctx.lineTo(x,y+height-radius);
                      ctx.quadraticCurveTo(x,y+height,x+radius,y+height);
                      ctx.lineTo(x+width-radius,y+height);
                      ctx.quadraticCurveTo(x+width,y+height,x+width,y+height-radius);
                      ctx.lineTo(x+width,y+radius);
                      ctx.quadraticCurveTo(x+width,y,x+width-radius,y);
                      ctx.lineTo(x+radius,y);
                      ctx.quadraticCurveTo(x,y,x,y+radius);
                      ctx.stroke();
                    }



                    function draw() {
 // ctx.fillStyle('rgb(12,24,33)');
  //s_rect(WIDTH/2 - width_rectangle/2, HEIGHT/2 - height_recatngle/2, width_rectangle, height_recatngle);
  for (var i = 0; i <500; i++) {
    // width_rectangle += i;
    // height_rectangle += i;
    console.log(points);
    roundedRect(ctx,(WIDTH/2-width_rectangle/2) + parseInt(18*points[i].X),HEIGHT/2-height_rectangle/2 + parseInt(18*points[i].Y),width_rectangle,height_rectangle,12);
  }}
  init();
}
}
/*MOD
make possible to paint on the image
*/
WordOnTopModPaint = function(settings){
  $(document).ready(function() {

  // Variables :
  var color = "#000";
  var painting = false;
  var started = false;
  var width_brush = 5;
  var canvas = settings.$canvas;
  var cursorX, cursorY;
  var restoreCanvasArray = [];
  var restoreCanvasIndex = 0;
  
  var context = canvas[0].getContext('2d');
  var $colors = $("<ul id='couleurs' style='display : relative;z-index : 99;float:left;magin-left :"+settings.width+"px;margin-top : -"+100+"px;'>\
    <li><a href='#' data-couleur='#000000' class='actif' style='background-color: rgb(0, 0, 0); background-position: initial initial; background-repeat: initial initial; '>Noir</a></li>\
    <li><a href='#' data-couleur='#ffffff' style='background-color: rgb(255, 255, 255); background-position: initial initial; background-repeat: initial initial; '>Blanc</a></li>\
    <li><a href='#' data-couleur='#ff0000' style='background-color: rgb(255, 0, 0); background-position: initial initial; background-repeat: initial initial; '>Rouge</a></li>\
    <li><a href='#' data-couleur='brown' style='background-color: rgb(165, 42, 42); background-position: initial initial; background-repeat: initial initial; '>Marron</a></li>\
    <li><a href='#' data-couleur='orange' style='background-color: orange; background-position: initial initial; background-repeat: initial initial; '>Orange</a></li>\
    <li><a href='#' data-couleur='yellow' style='background-color: yellow; background-position: initial initial; background-repeat: initial initial; '>Jaune</a></li>\
    <li><a href='#' data-couleur='green' style='background-color: green; background-position: initial initial; background-repeat: initial initial; '>Vert</a></li>\
    <li><a href='#' data-couleur='cyan' style='background-color: rgb(0, 255, 255); background-position: initial initial; background-repeat: initial initial; '>Cyan</a></li>\
    <li><a href='#' data-couleur='blue' style='background-color: blue; background-position: initial initial; background-repeat: initial initial; '>Bleu</a></li>\
    <li><a href='#' data-couleur='indigo' style='background-color: rgb(75, 0, 130); background-position: initial initial; background-repeat: initial initial; '>Indigo</a></li>\
    <li><a href='#' data-couleur='Violet' style='background-color: rgb(238, 130, 238); background-position: initial initial; background-repeat: initial initial; '>Violet</a></li>\
    <li><a href='#' data-couleur='pink' style='background-color: rgb(255, 192, 203); background-position: initial initial; background-repeat: initial initial; '>Rose</a></li>\
    </ul>");

settings.$global.parent().append($colors);
  // Trait arrondi :
  context.lineJoin = 'round';
  context.lineCap = 'round';
  
  // Click souris enfoncé sur le canvas, je dessine :
  canvas.mousedown(function(e) {
    painting = true;
    
    // Coordonnées de la souris :
    cursorX = (e.pageX - this.offsetLeft);
    cursorY = (e.pageY - this.offsetTop);
  });
  
  // Relachement du Click sur tout le document, j'arrête de dessiner :
  $(this).mouseup(function() {
    painting = false;
    started = false;
  });
  
  // Mouvement de la souris sur le canvas :
  canvas.mousemove(function(e) {
    // Si je suis en train de dessiner (click souris enfoncé) :
    if (painting) {
      // Set Coordonnées de la souris :
      cursorX = (e.pageX - this.offsetLeft) - 10; // 10 = décalage du curseur
      cursorY = (e.pageY - this.offsetTop) - 10;
      
      // Dessine une ligne :
      drawLine();
    }
  });
  
  // Fonction qui dessine une ligne :
  function drawLine() {
    // Si c'est le début, j'initialise
    if (!started) {
      // Je place mon curseur pour la première fois :
      context.beginPath();
      context.moveTo(cursorX, cursorY);
      started = true;
    } 
    // Sinon je dessine
    else {
      context.lineTo(cursorX, cursorY);
      context.strokeStyle = color;
      context.lineWidth = width_brush;
      context.stroke();
    }
  }
  
  // Clear du Canvas :
  function clear_canvas() {
    context.clearRect(0,0, canvas.width(), canvas.height());
  }
  
  // Pour chaque carré de couleur :

  
  // Largeur du pinceau :
  
  
  // Bouton Save :
  $("#save").click(function() {
    var canvas_tmp = document.getElementById("canvas"); // Ca merde en pernant le selecteur jQuery
    window.location = canvas_tmp.toDataURL("image/png");
  });
  
});
}

/*MOD
make a kaleidoscope on picture
*/
WordOnTopModKaleidoscope = function(settings){
  var imgLoaded = false;
  var ctx, img;
  Kaleidoscopex = 0;
  Kaleidoscopey = 0;
  var self = this;

  $(document).ready(function () {

    if (location.hash == '#dark') {
      $('body').addClass('dark');
    }

    // load the kaleidoscope
    loadNewKaleidoscope();

    // set up mouse events
   // mouseEvents();

 });


/* ---------------------------------------------
handle all mouse events
------------------------------------------------*/
function mouseEvents() {

    // set up mouse movement
    settings.$canvas.on('mousemove', function(e) {
      if (window.imgLoaded) {
        var offset = settings.$canvas.offset();
        var x = e.pageX - offset.left;
        var y = e.pageY - offset.top;
        drawKaleidoscope(window.ctx, window.img, x / 2, y / 2);
      }
    });

    // allow user to load new photos
    $('#getPhotos').live('click',function() {
      loadPhotosAndTags();
      return false;
    });

    // filtering loaded photos
    $('#filterByTagButton').click(function() {
      getFlickrPhotos(1);
      return false;
    });

    // permalink box
    $("#permalinkInput").click(function(){
      $(this).select();
    });

    // refresh page
    $('.refreshPage').live('click',function(){
      location.reload();
    });

    // toggle background
    $('#vToggleBg').click(function(){
      toggleBg();
      return false;
    });
  }


/* ---------------------------------------------
 Loads a new kaleidoscope
 ------------------------------------------------*/
 function loadNewKaleidoscope() {
  var canvas = settings.$canvas[0];
  window.ctx = canvas.getContext('2d');
  window.img = settings.$new[0];
  console.log( window.img );
  window.imgLoaded = false;
  window.Kaleidoscopex=0;
  window.Kaleidoscopey =0;

  setInterval(function(){
   window.Kaleidoscopex += 2;
   window.Kaleidoscopey += 2;
   console.log(  window.Kaleidoscopey);
   drawKaleidoscope(window.ctx, window.img,parseInt( window.Kaleidoscopex / 2), parseInt(window.Kaleidoscopey / 2));
 },50);

  window.imgLoaded = true;
  //drawKaleidoscope(window.ctx, window.img, 0, 0);

}


/* ---------------------------------------------
 Draws a kaleidoscope
 ------------------------------------------------*/
 function drawKaleidoscope(ctx, img, imgX, imgY) {
  try {
    var sqSide = settings.width/2;
    var sqDiag = Math.sqrt(2 * sqSide * sqSide);
    var maskSide = settings.width;
    if (img.height < img.width) {
      maskSide = Math.abs(img.height - sqDiag);
    } else {
      maskSide = Math.abs(img.width - sqDiag);
    }
    var c = settings.width/2;
    ctx.clearRect(0, 0, 500, 500);
        //7 (1)
        ctx.save();
        ctx.translate(c, c);
        ctx.rotate(-90 * (Math.PI / 180));
        ctx.scale(-1, -1);
        ctx.drawImage(img, imgX, imgY, maskSide, maskSide, 0, 0, sqSide, sqSide);
        ctx.restore();
        //2 (4)
        ctx.save();
        ctx.translate(c, c);
        ctx.rotate(-90 * (Math.PI / 180));
        ctx.scale(1, -1);
        ctx.drawImage(img, imgX, imgY, maskSide, maskSide, 0, 0, sqSide, sqSide);
        ctx.restore();
        //3 (5)
        ctx.save();
        ctx.translate(c, c);
        ctx.rotate(-90 * (Math.PI / 180));
        ctx.scale(1, 1);
        ctx.drawImage(img, imgX, imgY, maskSide, maskSide, 0, 0, sqSide, sqSide);
        ctx.restore();
        //8
        ctx.save();
        ctx.translate(c, c);
        ctx.rotate(-90 * (Math.PI / 180));
        ctx.scale(-1, 1);
        ctx.drawImage(img, imgX, imgY, maskSide, maskSide, 0, 0, sqSide, sqSide);
        ctx.restore();
        //1
        ctx.save();
        ctx.moveTo(c, c);
        ctx.lineTo(c - sqSide, c);
        ctx.lineTo(c - sqSide, c - sqSide);
        ctx.lineTo(c, c);
        ctx.clip();
        ctx.translate(c, c);
        ctx.scale(-1, -1);
        ctx.drawImage(img, imgX, imgY, maskSide, maskSide, 0, 0, sqSide, sqSide);
        ctx.restore();
        //4
        ctx.save();
        ctx.moveTo(c, c);
        ctx.lineTo(c + sqSide, c - sqSide);
        ctx.lineTo(c + sqSide, c);
        ctx.lineTo(c, c);
        ctx.clip();
        ctx.translate(c, c);
        ctx.scale(1, -1);
        ctx.drawImage(img, imgX, imgY, maskSide, maskSide, 0, 0, sqSide, sqSide);
        ctx.restore();
        //5
        ctx.save();
        ctx.moveTo(c, c);
        ctx.lineTo(c + sqSide, c);
        ctx.lineTo(c + sqSide, c + sqSide);
        ctx.lineTo(c, c);
        ctx.clip();
        ctx.translate(c, c);
        ctx.scale(1, 1);
        ctx.drawImage(img, imgX, imgY, maskSide, maskSide, 0, 0, sqSide, sqSide);
        ctx.restore();
        //8
        ctx.save();
        ctx.moveTo(c, c);
        ctx.lineTo(c - sqSide, c + sqSide);
        ctx.lineTo(c - sqSide, c);
        ctx.lineTo(c, c);
        ctx.clip();
        ctx.translate(c, c);
        ctx.scale(-1, 1);
        ctx.drawImage(img, imgX, imgY, maskSide, maskSide, 0, 0, sqSide, sqSide);
        ctx.restore();
      } catch(err) {
        $('#currentImage').remove();
        img = '';
        setLoadingMessage('drawingError');
        $('#loadingContainer').show();
        ctx.clearRect(0, 0, 500, 500);
      }
    }


/* ---------------------------------------------
 Manage loading messages
 ------------------------------------------------*/

 function setLoadingMessage(message) {
  if (message == 'default') {
    $('#loadingContainer .loading').html('Loading...<br/><img src="loader.gif" width="32" height="32" alt="loading"/>');
  } else if (message == 'loadingError') {
    $('#loadingContainer .loading').html('Sorry. That particular image is unavailable for some reason. <a href="#" class="openPhotoChooser">Please choose another one.</a>');
  } else if (message == 'drawingError') {
    $('#loadingContainer .loading').html('Sorry. There has been a problem loading that image. You could try <a href="#" class="refreshPage">refreshing the page</a> or maybe just <a href="#" class="openPhotoChooser">choose a different photo.</a>');
  } else {
    $('#loadingContainer .loading').html(message);
  }
}


/* ---------------------------------------------
 Manage loading messages
 ------------------------------------------------*/

 function toggleBg() {
  var $body = $('body');
  if ($body.hasClass('dark')) {
    location.hash = '';
    $body.removeClass('dark');
  } else {
    location.hash = 'dark';
    $body.addClass('dark');
  }
}

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*MOD
effect to make a gaussian blur on the image
*/
WordOnTopModBlur = function(settings,parameters){


  var mul_table = [
  512,512,456,512,328,456,335,512,405,328,271,456,388,335,292,512,
  454,405,364,328,298,271,496,456,420,388,360,335,312,292,273,512,
  482,454,428,405,383,364,345,328,312,298,284,271,259,496,475,456,
  437,420,404,388,374,360,347,335,323,312,302,292,282,273,265,512,
  497,482,468,454,441,428,417,405,394,383,373,364,354,345,337,328,
  320,312,305,298,291,284,278,271,265,259,507,496,485,475,465,456,
  446,437,428,420,412,404,396,388,381,374,367,360,354,347,341,335,
  329,323,318,312,307,302,297,292,287,282,278,273,269,265,261,512,
  505,497,489,482,475,468,461,454,447,441,435,428,422,417,411,405,
  399,394,389,383,378,373,368,364,359,354,350,345,341,337,332,328,
  324,320,316,312,309,305,301,298,294,291,287,284,281,278,274,271,
  268,265,262,259,257,507,501,496,491,485,480,475,470,465,460,456,
  451,446,442,437,433,428,424,420,416,412,408,404,400,396,392,388,
  385,381,377,374,370,367,363,360,357,354,350,347,344,341,338,335,
  332,329,326,323,320,318,315,312,310,307,304,302,299,297,294,292,
  289,287,(parseInt(settings.width/2)),282,280,278,275,273,271,269,267,265,263,261,259];


  var shg_table = [
  9, 11, 12, 13, 13, 14, 14, 15, 15, 15, 15, 16, 16, 16, 16, 17, 
  17, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 18, 18, 18, 19, 
  19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 20, 20, 20,
  20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 21,
  21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
  21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 
  22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22,
  22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 23, 
  23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23,
  23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23,
  23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 
  23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 
  24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
  24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
  24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
  24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24 ];

  function stackBlurImage(radius, blurAlphaChannel )
  {

    var img = settings.$new[0];
    var w = img.naturalWidth;
    var h = img.naturalHeight;

    WordOnTop_canvas = settings.$canvas[0];

    WordOnTop_canvas.style.width  = w + "px";
    WordOnTop_canvas.style.height = h + "px";
    WordOnTop_canvas.width = w;
    WordOnTop_canvas.height = h;
    
    var context = WordOnTop_canvas.getContext("2d");
    context.clearRect( 0, 0, w, h );
    context.drawImage( img, 0, 0 );

    if ( isNaN(radius) || radius < 1 ) return;

    if ( blurAlphaChannel )
      stackBlurCanvasRGBA(0, 0, w, h, radius );
    else 
      stackBlurCanvasRGB(0, 0, w, h, radius );
  }


  function stackBlurCanvasRGBA(top_x, top_y, width, height, radius )
  {
    if ( isNaN(radius) || radius < 1 ) return;
    radius |= 0;

    var canvas  = settings.$canvas[0];
    var context = canvas.getContext("2d");
    var imageData;

    try {
      try {
        imageData = context.getImageData( top_x, top_y, width, height );
      } catch(e) {

    // NOTE: this part is supposedly only needed if you want to work with local files
    // so it might be okay to remove the whole try/catch block and just use
    // imageData = context.getImageData( top_x, top_y, width, height );
    try {
      netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
      imageData = context.getImageData( top_x, top_y, width, height );
    } catch(e) {
      alert("Cannot access local image");
      throw new Error("unable to access local image data: " + e);
      return;
    }
  }
} catch(e) {
  alert("Cannot access image");
  throw new Error("unable to access image data: " + e);
}

var pixels = imageData.data;

var x, y, i, p, yp, yi, yw, r_sum, g_sum, b_sum, a_sum, 
r_out_sum, g_out_sum, b_out_sum, a_out_sum,
r_in_sum, g_in_sum, b_in_sum, a_in_sum, 
pr, pg, pb, pa, rbs;

var div = radius + radius + 1;
var w4 = width << 2;
var widthMinus1  = width - 1;
var heightMinus1 = height - 1;
var radiusPlus1  = radius + 1;
var sumFactor = radiusPlus1 * ( radiusPlus1 + 1 ) / 2;

var stackStart = new BlurStack();
var stack = stackStart;
for ( i = 1; i < div; i++ )
{
  stack = stack.next = new BlurStack();
  if ( i == radiusPlus1 ) var stackEnd = stack;
}
stack.next = stackStart;
var stackIn = null;
var stackOut = null;

yw = yi = 0;

var mul_sum = mul_table[radius];
var shg_sum = shg_table[radius];

for ( y = 0; y < height; y++ )
{
  r_in_sum = g_in_sum = b_in_sum = a_in_sum = r_sum = g_sum = b_sum = a_sum = 0;

  r_out_sum = radiusPlus1 * ( pr = pixels[yi] );
  g_out_sum = radiusPlus1 * ( pg = pixels[yi+1] );
  b_out_sum = radiusPlus1 * ( pb = pixels[yi+2] );
  a_out_sum = radiusPlus1 * ( pa = pixels[yi+3] );

  r_sum += sumFactor * pr;
  g_sum += sumFactor * pg;
  b_sum += sumFactor * pb;
  a_sum += sumFactor * pa;

  stack = stackStart;

  for( i = 0; i < radiusPlus1; i++ )
  {
    stack.r = pr;
    stack.g = pg;
    stack.b = pb;
    stack.a = pa;
    stack = stack.next;
  }

  for( i = 1; i < radiusPlus1; i++ )
  {
    p = yi + (( widthMinus1 < i ? widthMinus1 : i ) << 2 );
    r_sum += ( stack.r = ( pr = pixels[p])) * ( rbs = radiusPlus1 - i );
    g_sum += ( stack.g = ( pg = pixels[p+1])) * rbs;
    b_sum += ( stack.b = ( pb = pixels[p+2])) * rbs;
    a_sum += ( stack.a = ( pa = pixels[p+3])) * rbs;

    r_in_sum += pr;
    g_in_sum += pg;
    b_in_sum += pb;
    a_in_sum += pa;

    stack = stack.next;
  }


  stackIn = stackStart;
  stackOut = stackEnd;
  for ( x = 0; x < width; x++ )
  {
    pixels[yi+3] = pa = (a_sum * mul_sum) >> shg_sum;
    if ( pa != 0 )
    {
      pa = 255 / pa;
      pixels[yi]   = ((r_sum * mul_sum) >> shg_sum) * pa;
      pixels[yi+1] = ((g_sum * mul_sum) >> shg_sum) * pa;
      pixels[yi+2] = ((b_sum * mul_sum) >> shg_sum) * pa;
    } else {
      pixels[yi] = pixels[yi+1] = pixels[yi+2] = 0;
    }

    r_sum -= r_out_sum;
    g_sum -= g_out_sum;
    b_sum -= b_out_sum;
    a_sum -= a_out_sum;

    r_out_sum -= stackIn.r;
    g_out_sum -= stackIn.g;
    b_out_sum -= stackIn.b;
    a_out_sum -= stackIn.a;

    p =  ( yw + ( ( p = x + radius + 1 ) < widthMinus1 ? p : widthMinus1 ) ) << 2;

    r_in_sum += ( stackIn.r = pixels[p]);
    g_in_sum += ( stackIn.g = pixels[p+1]);
    b_in_sum += ( stackIn.b = pixels[p+2]);
    a_in_sum += ( stackIn.a = pixels[p+3]);

    r_sum += r_in_sum;
    g_sum += g_in_sum;
    b_sum += b_in_sum;
    a_sum += a_in_sum;

    stackIn = stackIn.next;

    r_out_sum += ( pr = stackOut.r );
    g_out_sum += ( pg = stackOut.g );
    b_out_sum += ( pb = stackOut.b );
    a_out_sum += ( pa = stackOut.a );

    r_in_sum -= pr;
    g_in_sum -= pg;
    b_in_sum -= pb;
    a_in_sum -= pa;

    stackOut = stackOut.next;

    yi += 4;
  }
  yw += width;
}


for ( x = 0; x < width; x++ )
{
  g_in_sum = b_in_sum = a_in_sum = r_in_sum = g_sum = b_sum = a_sum = r_sum = 0;

  yi = x << 2;
  r_out_sum = radiusPlus1 * ( pr = pixels[yi]);
  g_out_sum = radiusPlus1 * ( pg = pixels[yi+1]);
  b_out_sum = radiusPlus1 * ( pb = pixels[yi+2]);
  a_out_sum = radiusPlus1 * ( pa = pixels[yi+3]);

  r_sum += sumFactor * pr;
  g_sum += sumFactor * pg;
  b_sum += sumFactor * pb;
  a_sum += sumFactor * pa;

  stack = stackStart;

  for( i = 0; i < radiusPlus1; i++ )
  {
    stack.r = pr;
    stack.g = pg;
    stack.b = pb;
    stack.a = pa;
    stack = stack.next;
  }

  yp = width;

  for( i = 1; i <= radius; i++ )
  {
    yi = ( yp + x ) << 2;

    r_sum += ( stack.r = ( pr = pixels[yi])) * ( rbs = radiusPlus1 - i );
    g_sum += ( stack.g = ( pg = pixels[yi+1])) * rbs;
    b_sum += ( stack.b = ( pb = pixels[yi+2])) * rbs;
    a_sum += ( stack.a = ( pa = pixels[yi+3])) * rbs;

    r_in_sum += pr;
    g_in_sum += pg;
    b_in_sum += pb;
    a_in_sum += pa;

    stack = stack.next;
    
    if( i < heightMinus1 )
    {
      yp += width;
    }
  }

  yi = x;
  stackIn = stackStart;
  stackOut = stackEnd;
  for ( y = 0; y < height; y++ )
  {
    p = yi << 2;
    pixels[p+3] = pa = (a_sum * mul_sum) >> shg_sum;
    if ( pa > 0 )
    {
      pa = 255 / pa;
      pixels[p]   = ((r_sum * mul_sum) >> shg_sum ) * pa;
      pixels[p+1] = ((g_sum * mul_sum) >> shg_sum ) * pa;
      pixels[p+2] = ((b_sum * mul_sum) >> shg_sum ) * pa;
    } else {
      pixels[p] = pixels[p+1] = pixels[p+2] = 0;
    }

    r_sum -= r_out_sum;
    g_sum -= g_out_sum;
    b_sum -= b_out_sum;
    a_sum -= a_out_sum;

    r_out_sum -= stackIn.r;
    g_out_sum -= stackIn.g;
    b_out_sum -= stackIn.b;
    a_out_sum -= stackIn.a;

    p = ( x + (( ( p = y + radiusPlus1) < heightMinus1 ? p : heightMinus1 ) * width )) << 2;

    r_sum += ( r_in_sum += ( stackIn.r = pixels[p]));
    g_sum += ( g_in_sum += ( stackIn.g = pixels[p+1]));
    b_sum += ( b_in_sum += ( stackIn.b = pixels[p+2]));
    a_sum += ( a_in_sum += ( stackIn.a = pixels[p+3]));

    stackIn = stackIn.next;

    r_out_sum += ( pr = stackOut.r );
    g_out_sum += ( pg = stackOut.g );
    b_out_sum += ( pb = stackOut.b );
    a_out_sum += ( pa = stackOut.a );

    r_in_sum -= pr;
    g_in_sum -= pg;
    b_in_sum -= pb;
    a_in_sum -= pa;

    stackOut = stackOut.next;

    yi += width;
  }
}

context.putImageData( imageData, top_x, top_y );

}


function stackBlurCanvasRGB(top_x, top_y, width, height, radius )
{
  if ( isNaN(radius) || radius < 1 ) return;
  radius |= 0;
  
  var canvas  = settings.$canvas[0];
  var context = canvas.getContext("2d");
  var imageData;
  
  try {
    try {
      imageData = context.getImageData( top_x, top_y, width, height );
    } catch(e) {

    // NOTE: this part is supposedly only needed if you want to work with local files
    // so it might be okay to remove the whole try/catch block and just use
    // imageData = context.getImageData( top_x, top_y, width, height );
    try {
      netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
      imageData = context.getImageData( top_x, top_y, width, height );
    } catch(e) {
      alert("Cannot access local image");
      throw new Error("unable to access local image data: " + e);
      return;
    }
  }
} catch(e) {
  alert("Cannot access image");
  throw new Error("unable to access image data: " + e);
}

var pixels = imageData.data;

var x, y, i, p, yp, yi, yw, r_sum, g_sum, b_sum,
r_out_sum, g_out_sum, b_out_sum,
r_in_sum, g_in_sum, b_in_sum,
pr, pg, pb, rbs;

var div = radius + radius + 1;
var w4 = width << 2;
var widthMinus1  = width - 1;
var heightMinus1 = height - 1;
var radiusPlus1  = radius + 1;
var sumFactor = radiusPlus1 * ( radiusPlus1 + 1 ) / 2;

var stackStart = new BlurStack();
var stack = stackStart;
for ( i = 1; i < div; i++ )
{
  stack = stack.next = new BlurStack();
  if ( i == radiusPlus1 ) var stackEnd = stack;
}
stack.next = stackStart;
var stackIn = null;
var stackOut = null;

yw = yi = 0;

var mul_sum = mul_table[radius];
var shg_sum = shg_table[radius];

for ( y = 0; y < height; y++ )
{
  r_in_sum = g_in_sum = b_in_sum = r_sum = g_sum = b_sum = 0;

  r_out_sum = radiusPlus1 * ( pr = pixels[yi] );
  g_out_sum = radiusPlus1 * ( pg = pixels[yi+1] );
  b_out_sum = radiusPlus1 * ( pb = pixels[yi+2] );

  r_sum += sumFactor * pr;
  g_sum += sumFactor * pg;
  b_sum += sumFactor * pb;

  stack = stackStart;

  for( i = 0; i < radiusPlus1; i++ )
  {
    stack.r = pr;
    stack.g = pg;
    stack.b = pb;
    stack = stack.next;
  }

  for( i = 1; i < radiusPlus1; i++ )
  {
    p = yi + (( widthMinus1 < i ? widthMinus1 : i ) << 2 );
    r_sum += ( stack.r = ( pr = pixels[p])) * ( rbs = radiusPlus1 - i );
    g_sum += ( stack.g = ( pg = pixels[p+1])) * rbs;
    b_sum += ( stack.b = ( pb = pixels[p+2])) * rbs;

    r_in_sum += pr;
    g_in_sum += pg;
    b_in_sum += pb;

    stack = stack.next;
  }


  stackIn = stackStart;
  stackOut = stackEnd;
  for ( x = 0; x < width; x++ )
  {
    pixels[yi]   = (r_sum * mul_sum) >> shg_sum;
    pixels[yi+1] = (g_sum * mul_sum) >> shg_sum;
    pixels[yi+2] = (b_sum * mul_sum) >> shg_sum;

    r_sum -= r_out_sum;
    g_sum -= g_out_sum;
    b_sum -= b_out_sum;

    r_out_sum -= stackIn.r;
    g_out_sum -= stackIn.g;
    b_out_sum -= stackIn.b;

    p =  ( yw + ( ( p = x + radius + 1 ) < widthMinus1 ? p : widthMinus1 ) ) << 2;

    r_in_sum += ( stackIn.r = pixels[p]);
    g_in_sum += ( stackIn.g = pixels[p+1]);
    b_in_sum += ( stackIn.b = pixels[p+2]);

    r_sum += r_in_sum;
    g_sum += g_in_sum;
    b_sum += b_in_sum;

    stackIn = stackIn.next;

    r_out_sum += ( pr = stackOut.r );
    g_out_sum += ( pg = stackOut.g );
    b_out_sum += ( pb = stackOut.b );

    r_in_sum -= pr;
    g_in_sum -= pg;
    b_in_sum -= pb;

    stackOut = stackOut.next;

    yi += 4;
  }
  yw += width;
}


for ( x = 0; x < width; x++ )
{
  g_in_sum = b_in_sum = r_in_sum = g_sum = b_sum = r_sum = 0;

  yi = x << 2;
  r_out_sum = radiusPlus1 * ( pr = pixels[yi]);
  g_out_sum = radiusPlus1 * ( pg = pixels[yi+1]);
  b_out_sum = radiusPlus1 * ( pb = pixels[yi+2]);

  r_sum += sumFactor * pr;
  g_sum += sumFactor * pg;
  b_sum += sumFactor * pb;

  stack = stackStart;

  for( i = 0; i < radiusPlus1; i++ )
  {
    stack.r = pr;
    stack.g = pg;
    stack.b = pb;
    stack = stack.next;
  }

  yp = width;

  for( i = 1; i <= radius; i++ )
  {
    yi = ( yp + x ) << 2;

    r_sum += ( stack.r = ( pr = pixels[yi])) * ( rbs = radiusPlus1 - i );
    g_sum += ( stack.g = ( pg = pixels[yi+1])) * rbs;
    b_sum += ( stack.b = ( pb = pixels[yi+2])) * rbs;

    r_in_sum += pr;
    g_in_sum += pg;
    b_in_sum += pb;

    stack = stack.next;
    
    if( i < heightMinus1 )
    {
      yp += width;
    }
  }

  yi = x;
  stackIn = stackStart;
  stackOut = stackEnd;
  for ( y = 0; y < height; y++ )
  {
    p = yi << 2;
    pixels[p]   = (r_sum * mul_sum) >> shg_sum;
    pixels[p+1] = (g_sum * mul_sum) >> shg_sum;
    pixels[p+2] = (b_sum * mul_sum) >> shg_sum;

    r_sum -= r_out_sum;
    g_sum -= g_out_sum;
    b_sum -= b_out_sum;

    r_out_sum -= stackIn.r;
    g_out_sum -= stackIn.g;
    b_out_sum -= stackIn.b;

    p = ( x + (( ( p = y + radiusPlus1) < heightMinus1 ? p : heightMinus1 ) * width )) << 2;

    r_sum += ( r_in_sum += ( stackIn.r = pixels[p]));
    g_sum += ( g_in_sum += ( stackIn.g = pixels[p+1]));
    b_sum += ( b_in_sum += ( stackIn.b = pixels[p+2]));

    stackIn = stackIn.next;

    r_out_sum += ( pr = stackOut.r );
    g_out_sum += ( pg = stackOut.g );
    b_out_sum += ( pb = stackOut.b );

    r_in_sum -= pr;
    g_in_sum -= pg;
    b_in_sum -= pb;

    stackOut = stackOut.next;

    yi += width;
  }
}

context.putImageData( imageData, top_x, top_y );

}

function BlurStack()
{
  this.r = 0;
  this.g = 0;
  this.b = 0;
  this.a = 0;
  this.next = null;
}
var blurRadius;
if(!parameters){
  wordontop_blurRadiusBegin  = 23;
  wordontop_blurRadiusEnd = 100;
  
}else{
  if (parameters.constructor == Array){
    wordontop_blurRadiusBegin = parameters[0];
    wordontop_blurRadiusEnd = parameters[1];
  }
  else{
    blurRadius = parameters;
  }}
  console.log('blur effect radius :');
  console.log(parameters);

  wordontop_blurRadius = wordontop_blurRadiusBegin;
  wordontop_intervalBlur = setInterval(function(){
   wordontop_blurRadius+= 24;
   if(wordontop_blurRadius < wordontop_blurRadiusEnd){

    stackBlurImage(wordontop_blurRadius, false );
  }
},600);

  setInterval(function(){
    if(wordontop_finish_loading){
      clearInterval(wordontop_intervalBlur);
      var ctx = WordOnTop_canvas.getContext("2d");
      WordOnTop_canvas.width = WordOnTop_canvas.width;
    }
  },1000);
};


WordOnTopModCloud = function(settings,parameters) 
     { // The canvas element we are drawing into.      
      var $canvas = settings.$canvas;
      var $canvas2 = $("<canvas style='display:none' id='canvas2' width="+parseInt(settings.width/2)+" height="+parseInt(settings.height/2)+"></canvas>");
      $('.wordontopclass').append($canvas2);
      var $canvas3 = $("<canvas style='display:none' id='canvas3' width="+settings.width+" height="+settings.height+"></canvas>");
      $('.wordontopclass').append($canvas3);
      
      var ctx2 = $canvas2[0].getContext('2d');
      var ctx = $canvas[0].getContext('2d');
      var w = $canvas[0].width, h = $canvas[0].height;    
      var img = new Image();  
      
      // A puff.
      var Puff = function(p) {        
        var opacity,
        sy = (Math.random()*(parseInt(settings.width/2)))>>0,
        sx = (Math.random()*(parseInt(settings.width/2)))>>0;
        
        this.p = p;
        
        this.move = function(timeFac) {           
          p = this.p + 0.3 * timeFac;       
          opacity = (Math.sin(p*0.05)*0.5);           
          if(opacity <0) {
            p = opacity = 0;
            sy = (Math.random()*(parseInt(settings.width/2)))>>0;
            sx = (Math.random()*(parseInt(settings.width/2)))>>0;
          }                       
          this.p = p;                                     
          ctx.globalAlpha = opacity;            
          ctx.drawImage($canvas3[0], sy+p, sy+p, (parseInt(settings.width/2))-(p*2),(parseInt(settings.width/2))-(p*2), 0,0, w, h);               
        };
      };
      
      var puffs = [];     
      var sortPuff = function(p1,p2) { return p1.p-p2.p; }; 
      puffs.push( new Puff(0) );
      puffs.push( new Puff(20) );
      puffs.push( new Puff(40) );
      
      var newTime, oldTime = 0, timeFac;
      
      var loop = function()
      {               
        newTime = new Date().getTime();       
        if(oldTime === 0 ) {
          oldTime=newTime;
        }
        timeFac = (newTime-oldTime) * 0.1;
        if(timeFac>3) {timeFac=3;}
        oldTime = newTime;            
        puffs.sort(sortPuff);             
        
        for(var i=0;i<puffs.length;i++)
        {
          puffs[i].move(timeFac); 
        }         
        ctx2.drawImage( $canvas[0] ,0,0,570,570);       
        setTimeout(loop, 10 );        
      };
      // Turns out Chrome is much faster doing bitmap work if the bitmap is in an existing canvas rather
      // than an IMG, VIDEO etc. So draw the big nebula image into canvas3
      var $canvas3 =$('#canvas3');
      var ctx3 = $canvas3[0].getContext('2d');
      console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
      console.log(settings.$new[0].src);
      img = settings.$new[0];
      setInterval(function(){
        if(wordontop_finish_loading){
          clearInterval(wordontop_intervalBlur);
          var ctx = WordOnTop_canvas.getContext("2d");
          WordOnTop_canvas.width = WordOnTop_canvas.width;
        }
      },1000);
      ctx3.drawImage(img, 0,0, settings.width, settings.height);  loop();
      
    }




    WordOnTopModStamp= function(settings,parameters) 
    { 
      var source_height,source_width;
      var $canvas = settings.$canvas;
      var ctx =$canvas[0].getContext('2d');
      window.wordontop_ctx = ctx;
      WordOnTop_canvas = $canvas;
      var oldTime=  0;
      window.wordontop_count_state = 0;

      var $canvas_buffer = $("<canvas style='display:none' id='canvas2' width="+parseInt(settings.width/2)+" height="+parseInt(settings.height/2)+"></canvas>");
      $('.wordontopclass').append($canvas_buffer);
      var ctx2 = $canvas_buffer[0].getContext('2d');
      WordOnTop_canvas = $canvas_buffer[0]
      source_height = parameters.height;
      source_width = parameters.width;
      var offset = parameters.offset;
      var length_x = Math.floor(settings.height/parameters.height);
      var length_y = Math.floor(settings.width/parameters.width);


      var list_pos =new  Array(length_x);
      for (var i = 0; i < length_x; i++) {
        list_pos[i] = new Array(length_y);
        for (var k = 0; k <= length_y-1;k++) {
          list_pos[i][k] = false;
        }
      }

      var Stamp = function(p) {        
        this.p = p;

        this.draw = function(timeFac) {           
          p = this.p + 0.3 * timeFac;       
          opacity = 1;           
          if(opacity <0) {
            p = opacity = 0;
          }                       
          this.p = p;                                     
          ctx.globalAlpha = opacity;      
          var random_position_x = Math.floor(Math.random()*(length_x-1));
          var random_position_y = Math.floor(Math.random()*(length_y-1));
          var random_direction_x = Math.ceil( Math.random()*2)-1;
          var random_direction_y =Math.ceil(Math.random()*2)-1;
          if(!list_pos[random_position_x][random_position_y]){
           console.log('dessine : '+stamp_sprite[0].src+' à x:'+parseInt(random_position_x*source_width)+' y:'+parseInt(random_position_y*source_height));
           console.log(length_y);
           ctx.drawImage(stamp_sprite[0],(random_direction_y  * Math.floor(Math.random()*offset)) + (random_position_y*source_height), (random_direction_x *  Math.floor(Math.random()*offset)) +  (random_position_x*source_width),source_height,source_width);               
           list_pos[random_position_x][random_position_y] = true;
         }
       };
     };
     var stamps = [];     
     var sortStamps = function(p1,p2) { return p1.p-p2.p; }; 
     var loop = function()
     {               
      newTime = new Date().getTime();       
      if(oldTime === 0 ) {
        oldTime=newTime;
      }
      timeFac = (newTime-oldTime) * 0.1;
      if(timeFac>3) {timeFac=3;}
      oldTime = newTime;            
      stamps.sort(sortStamps);             

      stamps.push(new Stamp(0));
      for(var i=0;i<stamps.length;i++)
      {
        wordontop_count_state++;
        ctx.save();  
        stamps[i].draw(timeFac); 
      }     
      if(!wordontop_finish_loading){
       setTimeout(loop, 200 );
     }
   };
   var background = settings.$new[0];
   var stamp_sprite = $(new Image());
   stamp_sprite.hide();
   settings.$new.parent().append(stamp_sprite);
   stamp_sprite.attr("src",parameters.src);
   $.ajax({ //other options here
    complete: function () {
   // change image back when ajax request is complete

   setInterval(function(){
    if(wordontop_finish_loading){
      WordOnTop_canvas.width = WordOnTop_canvas.width;
      for(var r = 0;r<wordontop_count_state;r++){
        console.log("nb restoration :"+r);
        window.wordontop_ctx.restore();
      }
    }
  },1000);
   ctx = $canvas[0].getContext('2d');

   ctx.drawImage(background, 0,0,settings.width, settings.height);
   loop();

 } });



 }