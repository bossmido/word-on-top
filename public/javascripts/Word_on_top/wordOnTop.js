/*
 * author : Thibaud Bernard for Nteo
 * dependencies : jquery 1.7.2,>
 */

 /** @define {boolean} */
 var WAS_COMPILED = false;
 /** @define {boolean} */
 var WAS_SUPER_COMPILED = false;
  var WORDONTOP_FINISH_LOADING = false
  var WORDONTOP_LOADING_ERROR = false;
 (function($) {
	/*
	 * example for use : img.wordontop({...}).filter(':caption'); ------> result : [{
	 * content : "...", x : 0, y : 0, font-family : '', color : '',
	 * 
	 * },...]
	 */
	 $.extend(jQuery.expr[":"], {
	 	caption : function(elem) {
	 		if ($(elem).hasClass('caption')) {
	 			return true;
	 		} else {
	 			return false;
	 		}
	 	}
	 });

	 $.fn.getStyleObject = function() {
	 	var dom = this.get(0);
	 	var style;
	 	var returns = {};
	 	if (window.getComputedStyle) {
	 		var camelize = function(a, b) {
	 			return b.toUpperCase();
	 		}
	 		style = window.getComputedStyle(dom, null);
	 		for ( var i = 0; i < style.length; i++) {
	 			var prop = style[i];
	 			var camel = prop.replace(/\-([a-z])/g, camelize);
	 			var val = style.getPropertyValue(prop);
	 			returns[camel] = val;
	 		}
	 		return returns;
	 	}
	 	if (dom.currentStyle) {
	 		style = dom.currentStyle;
	 		for ( var prop in style) {
	 			returns[prop] = style[prop];
	 		}
	 		return returns;
	 	}
	 	return this.css();
	 }
	})(jQuery);

	$(document)
	.ready(
		function() {
			(function($) {
				$.fn.autoGrowInput = function(o) {

					o = $.extend({
						maxWidth : 1000,
						minWidth : 0,
						comfortZone : 70
					}, o);

					this
					.filter('textarea,input')
					.each(
						function() {

							var minWidth = o.minWidth || $(this).width(), val = '', input = $(this), testSubject = $('<tester/>').css(
							{
								position : 'absolute',
								top : -9999,
								left : -9999,
								width : 'auto',
								fontSize : input
								.css('fontSize'),
								fontFamily : input
								.css('fontFamily'),
								fontWeight : input
								.css('fontWeight'),
								letterSpacing : input
								.css('letterSpacing'),
								whiteSpace : 'nowrap',
								display : 'none',
							}), totalHeight = $(this).height(), check = function() {
								if (val === (val = input.val())) {
									return;
								}
								testSubject
								.css({
									fontSize : input.css('fontSize')
								});
													// Enter new content into testSubject

													var escaped = val
													.replace(/&/g, '&amp;')
													.replace(/ /g,'&nbsp;')
													.replace(/</g,'&lt;').replace(/>/g,'&gt;');
													var resArr = escaped.split("\n");
													var max = 0;
													var index = 0;
													console.log(resArr);
													for (s in resArr) {
														if (resArr[s].toString().length > max) {
															max = resArr[s].length;
															index = s;
														}
													}
													console.log("wordontop : "+ wordontop_g._("indice")+ " : "+ index);
													testSubject.html(resArr[index]);
													var line_height = testSubject.height();
													totalHeight = line_height * (resArr.length);
													// Calculate new width 
													var testerWidth = testSubject.width(), newWidth = (testerWidth + o.comfortZone) >= minWidth ? testerWidth+ o.comfortZone : minWidth, currentWidth = input.width(), isValidWidthChange = ((newWidth < currentWidth && newWidth >= minWidth) || (newWidth > minWidth && newWidth < o.maxWidth)) && (input.val().length > 0);
													input.height(totalHeight);
													// Animate width
													if (isValidWidthChange) {
														input.width(newWidth);
														input.parent().click();
													}
													if (input.val().length > 0) {
														input.parent().height(input.outerHeight());
														input.parent().width(input.outerWidth());
														input.parent().parent().trigger('change_width');

													}
													if (input.val().length < 0) {
														input.height(line_height);
													}
												};

												testSubject.insertAfter(input);

												$(this).on('keyup keydown blur update',check);
											});

										return this;

										};

jQuery.fn.fontSelector = function() {

	var fonts = new Array(
		'Arial,Arial,Helvetica,sans-serif',
		'Arial Black,Arial Black,Gadget,sans-serif',
		'Comic Sans MS,Comic Sans MS,cursive',
		'Courier New,Courier New,Courier,monospace',
		'Georgia,Georgia,serif',
		'Impact,Charcoal,sans-serif',
		'Lucida Console,Monaco,monospace',
		'Lucida Sans Unicode,Lucida Grande,sans-serif',
		'Palatino Linotype,Book Antiqua,Palatino,serif',
		'Tahoma,Geneva,sans-serif',
		'Times New Roman,Times,serif',
		'Trebuchet MS,Helvetica,sans-serif',
		'Verdana,Geneva,sans-serif');

	$("body").click(function() {
		$(".fontselector").hide();
	});
	window.wordontop_ul = [];
	return this.each(function() {

			// Get input field
			var sel = this;
			console.log("@@@@@@@@@@@@@@@@");
	console.log("wordontop :choose font!");
						 
			// Add a ul to hold fonts
			var ul = $('<ul class="fontselector"></ul>');
			window.wordontop_ul[""+ $(this).parent().parent().attr('id')] = ul;
			$(this).parent().parent().append(ul);
			$(ul).hide();

			jQuery.each(fonts,
				function(i, item) {
					$(ul).append('<li><a href="#" class="font_'+ i+ '" style="font-family: '+ item+ '">'+ item.split(',')[0]+ '</a></li>');

								// Prevent real select to work
								$(sel).click(
									function(ev) {
										ev.preventDefault();
										// Show fontlist
										$(ul).show();
													// Position font list 
													$(ul).css(
													{
														top : $(sel).position().top + $(sel).height()+ 9,
														left : $(sel).position().left + 5
													});

													// Blur field
													$(this).blur();
													return false;
												});

								$(ul).find('a').click(
									function(ev) {
										ev.preventDefault();
										ev.stopPropagation();
										var font = fonts[$(this).attr('class').split('_')[1]];
										$(sel).val(font).css("font-family",font);
										$(sel).val(font).width($(sel).val(font).width());
										$(ul).hide();
										$(sel).trigger('change');
										return false;
									});

							});

});

}

							jQuery.fn.insertAt = function(index, element) {
								var lastIndex = this.children().size()
								if (index < 0) {
									index = Math.max(0, lastIndex + 1 + index)
								}
								this.append(element);
								if (index < lastIndex) {
									this.children().eq(index).before(
										this.children().last())
								}
								return this;
							}
						/**
						 * constructor
						 */
						 $.fn.wordOnTop = function(options) {
						 	var self = this;
						 	if (!$.browser.webkit) {
						 		console.log("wordontop : browser not supported!");
						 	}
						 	if (!(this.get(0).tagName.toLowerCase() == 'img')) {
						 		alert("plugin wordontop work only on img tag.");
						 	} else {

						 		var captionscons = function() {
						 			this.x = 0;
						 			this.y = 0;
						 			this.width = 0;
						 			this.height = 0;
						 			this['font-family'] = '';
						 			this['font-size'] = "";
						 			this.content = "";
						 			this['_id'] = "";
						 			this.nbcaption = 0;
						 		};

						 		captions = function() {
						 		};
						 		captions.prototype = new captionscons();

						 		function captionscons() {
						 			this.x = 0;
						 			this.y = 0;
						 			this.width = 0;
						 			this.height = 0;
						 			this['font-family'] = '';
						 			this['font-size'] = "";
						 			this.content = "";
						 			this['_id'] = "";
						 			this.nbcaption = 0;
						 		}

						 		function captions() {
						 		}
						 		
						 		captions.prototype = new captionscons();

						 		var captionsArr = [];
						 		var LOADING_SUCESS = 0;
						 		var LOADING_EMPTY = 1;
						 		var LOADING_ERROR = 2;
										// Create some defaults, extending them
										// with any options that were provided
										this.settings = $.fn.extend(
										{
											// src de l'image
											'id' : "",
											// id of eachcaption in the  plugin
											'nb_caption' : 0,
											// new jquery object of the image
											'$new' : null,
											// div of the plugin
											'$global' : null,
											// height of the image
											'height' : 0,
											// width of the image
											'width' : 0,
											// integer checking if loading is correct :
											//-> (0)correct
											//-> (1)create a new image in database
											//-> (2)fail
											'load' : 0,
											//object that contain the error given to the callback called in display
											'error' : {},
											//list of captions accessible by the image
											'captions' : captionsArr,
											//relative adress ofserver to POST the change 
											'post' : "",
											//relative adress of the server to get the captions
											'get' : "",
											/*
											*
											*margin to transfer from the iage to the global div .wordontopclass
											*
											*/
											'marginleft' : 0,
											'marginright' : 0,
											'margintop' : 0,
											'marginbottom' : 0,
											//list of module to load passing in parameter
											'module' : [],
											//the server adress need to be overriden  to work into bookmarklet (by default hardcoded to the development adress)
											'server_location' : "localhost:4000",
											//wip for random color in place to the transparency of the caption doesn't work anymore
											'random_color' : true,
											//possible values : -'en'
											//					-''
											'lang' : '',
											//wip dosent work too
											'simple' : false,
											/*
											*	3 choices possibles :
											*		-'purple'
											*		-'white'
											*		-''
											*/		
											'theme' : '',

										}, options);
										var settings = this.settings;
										var g = null;
										if(!settings.get || !settings.post)
										getServerLocation(settings);

										settings.server_location = 'http://'+settings.server_location;
										if (!WAS_SUPER_COMPILED) {
											if(settings.lang == 'fr')settings.lang = '';
											if (settings.lang != ''	&& !WAS_COMPILED) {
												initlib('pixel_effect.js',settings, true);
												initlib('text.js', settings,true);
												initlib('Gettext.js', settings,true);
												
												g = wordontop_g = new Gettext({
													domain : settings.lang,
													locale_data : wordontop_text
												});
												g._ = g.gettext;
											} else {
												g =  wordontop_g = {
													_ : function(s) {
														return s;
													}
												}
												wordontop_g = g;
											}

											if (!window.bookmarklet) {
												initlib(
													'jquery.simple-color.min.js',
													settings);
												initlib(
													'jquery.ddslick.min.js',
													settings);
												initlib(
													'jquery-ui-1.8.20.custom.min.js',
													settings);
												initlib(
													'jquery.ui.touch-punch.min.js',
													settings);
											}
										}

										// initlib('jquery.ui.fontSelector.min.js');

										settings.height = $(this).height();
										settings.width = $(this).width();
										console.log("wordontop : "+ g._('largeur') + ' '+ settings.width);
										var $new = $(this);
										settings.$new = $(this).wrap("<div class='wordontopclass' />");
										var wordontop_plugin = settings.$new.parent();
										var $trigger_area = $("<div class='trigger_area' style='margin-left : -20%;float : right;position : relative;z-index :60;width : 20%;height : 100%;'></div>");
										var $modal_list = $("<div class='wrapper_tree' style='position:absolute;' ><div class='tree' style='z-index : "+ (2)+ ";max-height :"+ settings.height+ ";padding :15px;text-align : center; display:inline-block;max-width :210px;background-color : #BDD4DE;min-height : 70px;border-radius :4px;margin-top:0px'><h1 style='border-bottom: 1px solid #AAA;'>"+wordontop_g._("liste des légendes")+" : </h1><ul></ul></div></div>");
										wordontop_plugin.append($modal_list);
										wordontop_plugin.append($trigger_area);
										$modal_list.css({
											'opacity' : 0,
										});
										$modal_list.css({

											'left' : (settings.$new.position().left + settings.width),
											'top' : (settings.$new.offset().top + 3),
											'min-width' : $modal_list.children().eq(0).width(),
											'overflow' : 'hidden',
										});

										$modal_list.children().eq(0).css('margin-left',$modal_list.width()+ $modal_list.children().eq(0).width()+ 'px');
										
										if (settings.$new.parent().parent().width() < ($modal_list.outerWidth() + settings.$new.outerWidth())) {
											settings.$new.parent().parent().width(($modal_list.outerWidth() + settings.$new.outerWidth()));
										}
										settings.$new.parent().css("max-width",settings.$new.parent().width());

										settings.marginleft = settings.$new.css('margin-left');
										settings.marginright = settings.$new.css('margin-right');
										var idx = settings.$new.index();
										var valtosum = parseInt(settings.$new.eq(idx).css('margin-top'), 10);
										if (!valtosum)
											valtosum = 0;
										settings.marginbottom = parseInt(settings.$new.css('margin-bottom'),10)+ valtosum;
										settings.margintop = settings.$new.css('margin-top');

										var source_tmp = settings.$new.attr('src');
										if (source_tmp[0] != 'h') {
											settings.src = window.location.host+ settings.$new.attr('src');
										} else {
											settings.src = settings.$new.attr('src');
										}

										var $global = settings.$new.parent();
										settings.$global = $global;
										wordontop_finish_loading = false;
										var methodspro ={
															tayau : self,
															// sub-method used  create the  element
															create : function(id,callback) {
																console.log("wordontop : "+ wordontop_g._("creation du plugin avec paramètre"));
																return this;
															},
															/**
															 * display the word  depending to the  JSON structure
															 * @param {Object}  id
															 * @param {Object}
															 *		json {
															 *            "width":512,
															 *            "height":512,
															 *            "caption" : [ {
															 *            	"x" : 10,
															 *            	"y" : 25,
															 *            	"width" : 150,px
															 *            	"height" : 50,
															 *            	"font-family": "helvetica,sans-serif",
															 *            	"color" : "#FFFFFF",
															 *            	"font-size" : "14px",
															 *            	"content" : "..." } ] }
															 * @param {Object}  callback
															 */

															 display : function(json,callback) {
															 	console.log("wordontop : "+ wordontop_g._("affichage du plugin avec paramètre"));
															 	if (typeof json == "string") {
															 		var obj = jQuery.parseJSON(json);
															 		delete json;
															 		json = obj;
															 	}
															 	console.log("wordontop : "+ wordontop_g._('json d\'update'));
															 	console.log("wordontop : "+ json);
															 	var cnt = 0;
															 	for (j in json.captions) {
															 		console.log("wordontop : "+ cnt);
															 		j = json.captions[cnt];
															 		if (typeof j != "undefined") {
															 			j.nbcaption = cnt;
															 			this.tayau.settings.$new.parent().trigger('wordontop.update',j);
															 			cnt++;
															 		}

															 	}
															 	this.tayau.settings.nb_caption = cnt;
															 	if (callback) {
															 		var msg = null;
															 		if (cnt < 1) {
															 			msg = new Error(wordontop_g._('pas de message à afficher'));
															 		}
															 		callback.call(msg);
															 	}
															 	return this;
															 },
															 update : function(id, json,callback) {
															 	console.log("wordontop : "+ wordontop_g._("mis a jour du plugin avec paramètre"));
															 	var obj = jQuery.parseJSON(json);
															 	var $el = $("bloc_caption"+ id);
															 	$el.children("#caption"+ id).val(obj.content ? obj.content: "");
															 	var $tmp = $el.children(".font").val(obj['font-family'] ? obj['font-family'] : "Impact,Charcoal,sans-serif");
															 	$tmp.trigger("change");
															 	var $tmp = $el.children(".simple_color").val(obj['color'] ? obj['color']: "#333333");
															 	$tmp.trigger("change");
															 	if(callback){
															 		callback(new Error(this.tayau.settings.error),obj);
															 	}

															 	return this;
															 },
															/**
															 ** delete list of captions on theserver
															 **/
															 deleteWordOnTop : function(id) {
															 	console.log("wordontop : "+ g._("efface le contenu du plugin"));
															 	$img_origin = id;
															 	$from = window.$('.wordontopclass').parent();
															 	window.$('.wordontopclass').empty();
															 	$from.append($img_origin);
															 	return this;
															 },
															 deleteCaption : function(id) {
															 	console.log("wordontop : "+ wordontop_g._("efface une legende en fonction de son id"));
															 	$("bloc_caption"+ id).remove();
															 	DELETE(null,tayau.settings.captions[$(this).parent().attr('id')]);
															 	delete tayau.settings.captions[$(this).parent().attr('id')];
															 	return this;
															 },
															 DELETE : function(adress,content) {
															 	var self = this;
															 	var send;
															 	if (content['_id']) {
															 		if (!adress) {
															 			adress = this.tayau.settings.post;
															 		}
															 		var nbcaption;
															 		if (content.nbcaption)
															 			nbcaption = content.nbcaption;
															 		if (content['_id']) {
															 			send = content['_id'];
															 		}

															 		$.ajax({
															 			type : 'DELETE',
															 			url : adress,
															 			data : {
															 				'_id' : send
															 			},
															 			success : function(data) {
															 				console.log("wordontop : "
															 					+ wordontop_g
															 					._('suppression du caption :')
															 					+ ' '
															 					+ nbcaption);
															 			},
															 			dataType : 'json'
															 		});
															 	}

															 	return this;

															 },
															 POST : function(adress,content) {
															 	var self = this;
															 	var id;
															 	self.content = content;
															 	if (content && 'nbcaption' in content)
															 		id = content.nbcaption;
															 	if (!adress) {
															 		adress = "";
															 	}
															 	adress += this.tayau.settings.post;
															 	if (!content) {
															 		content = JSONify(this.tayau.settings);
															 	}

															 	console.log("wordontop : "+ wordontop_g._("enregistre sur le serveur..."));
															 	console.log(content);
															 	console.log("wordontop : "+ adress);
															 	$.ajax({
															 		type : 'POST',
															 		url : adress,
															 		data : content,
															 		error : function(data) {
															 				WORDONTOP_LOADING_ERROR = true;
															 			$('#bloc_caption'+ id).find('.icon').css(
															 				'color',
															 				'red');
															 		},
															 		success : function(data) {
															 			console.log("wordontop : "+ wordontop_g._('enregistré'));
															 			console.log(self.tayau.settings.captions['bloc_caption'+ id]);
															 			if ('nbcaption' in self.content) {
															 				self.tayau.settings.captions['bloc_caption'+ id]._id = data.id;
															 				console.log(self.tayau.settings.captions['bloc_caption'+ id]);
															 				$('#bloc_caption'+ id).find('.icon').css('color','green');
															 			}
															 		},
															 		dataType : 'json'
															 	});

															 	return this;
															 },
															 GET : function(adress) {

															 	if (!adress) {
															 		adress = this.tayau.settings.server_location;
															 	}
															 	var self = this;
															 	var src = self.tayau.settings.src;
															 	console.log("wordontop : "+ wordontop_g._("récupère les données du serveur..."));
															 	var separator = "/";
															 	if (adress.substr(-1) !== "/") {
															 		separator = "/";
															 	}
															 	$.ajax({
															 		type : 'GET',
															 		url : adress+ this.tayau.settings.get+ encodeURIComponent(src),
															 		success : function(data) {
															 			console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@',data);
															 			if(data.caption === null){
																			self.tayau.settings.load = LOADING_ERROR;
															 				return;
															 			}
															 			if (data.captions.length > 0) {
															 				self.tayau.settings.load = LOADING_SUCESS;
															 			} else {
															 				self.tayau.settings.load = LOADING_EMPTY;
															 			}
															 			console.log("wordontop : "+ wordontop_g._("données récupèrées..."));
															 			console.log(data);
															 			self.display(data,null);
															 			wordontop_finish_loading = true;

															 			self.tayau.settings.$canvas[0].style.zIndex = 5;

															 		},
															 		xhr : function() {
															 			var xhr = new window.XMLHttpRequest();
																				// Upload  progress
																				xhr.upload
																				.addEventListener("progress",function(evt) {
																					if (evt.lengthComputable) {
																						var percentComplete = evt.loaded / evt.total;

																							// Do something with upload progress
																							console.log("wordontop : "+ percentComplete);
																						}
																					},
																					false);
																				// Download progress
																				xhr.addEventListener(
																					"progress",function(evt) {
																						if (evt.lengthComputable) {
																							var percentComplete = evt.loaded / evt.total;
																							// Do something with download progress
																							console.log("wordontop : "+ percentComplete);
																						}
																					},false);
																				return xhr;
																			},
																			error : function(data) {
																				WORDONTOP_LOADING_ERROR = true;
																				self.tayau.settings.load = LOADING_ERROR;
																				console.log("wordontop : ", data);
																				
																				$('#bloc_caption'+ id).find('.icon').css('color','red');
																			},
																			dataType : 'json'
																		});

													return this;

													}
													};
													
													methodspro.prototype = {};
													methodspro.prototype.GET = methodspro.GET;
													this.methodspro = methodspro;
										var methods= window['wordontop_public'] = $.extend(methodspro, wordontop_plugin);
										window['wordontop_jQuery_less'] = methodspro;


													redraw(settings);
										// makeAbsolute(settings);

										var $button_saveall = $("<input type='button' value="+ g._('sauvegarder tous les captions')+ "/>")
										// settings.$global.append($button_saveall);

										function messageAjax(msg) {
											settings.$new.parent().prepend(
												"<div id=\"infoBox\" class=\"progress-bar\" style=\"opacity : 0;text-font :  sans-serif;text-shadow : rgba(64,64,64,0.4) 1px 1px 1.8em;height: 20px;background-color:#ffffff;width-max:"
												+ settings.width
												+ ";display:none;text-align:center;border-bottom:#2BBAE4 1px solid;margin-left :"
												+ settings.marginleft
												+ ";margin-top : "
												+ settings.margintop
												+ "\"></div>");
											setTimeout(settings.$global.find("#infoBox").text(msg).slideDown().delay(3000).slideUp(), 0);
											var displacement = parseInt(settings.$canvas.css('margin-top'));
											settings.$canvas.animate(
											{
												'margin-top' : displacement + 40
											}, 2000,
											function() {

											}).animate(
											{
												'margin-top' : displacement
											}, 3000);

										}

										messageAjax(g
											._('plugin word on top chargé'));
										// //////////////////////////////////////////////////////////////:
										// handling image
										$('.close')
										.css(
										{
											'background-image' : "url('"
												+ settings.server_location
												+ "/public/stylesheets/close.svg'')"
									});

										// //////////////////////////////////////////////////////////////loading
										// async

										setTimeout(
											function() {
												if (settings.get != "")
													methods
												.GET(settings.server_location);
											}, 1000);
										setTimeout(
											function() {
												methods
												.POST(
													settings.server_location,
													{
														title : settings.src,
														width : settings.width,
														height : settings.height
													});
											}, 3000);
										// ///////////////////////////////////////////////////////////////loading
										// module
										if (settings.module != []) {
											for (f in settings.module) {
												if (settings.module[f]) {
													if (!(typeof settings.module[f] == 'string')) {
														window['WordOnTopMod'
														+ settings.module[f].name]
														(
															settings,
															settings.module[f].parameter);
													} else {
														if (settings.module[f] == 'Random') {
															var list_mod = [];
															var reg1 = new RegExp(
																"[a-z]*WordOnTopMod[a-z]*",
																"g");
															for (v in window) {
																
																if (window[v]
																	&& typeof window[v] === 'function'
																	&& v
																	.toString()
																	.match(
																		reg1)) {
																	list_mod
																.push(v
																	.toString());
																}

																}
																var fun = list_mod[Math.floor(Math.random()* (list_mod.length - 1))];
																window[fun](settings);
																} else {
																	window['WordOnTopMod'+ settings.module[f]](settings);
																}
													}
											}
								}
					}
										// /////////////////////////////////////////////////////////
										// handling settings default parameter

										// according to image size
										var default_size = {};
										default_size.zoom_toolbox = '100%';
										default_size.rule_readable = '0px 0px 0px #ccc'
										if (settings.width > 900) {
											default_size.fontSize = 32;
											default_size.captionHeight = 500;
											default_size.captionWidth = 500;
											default_size.padding = 60;
										} else if (settings.width > 500) {
											default_size.fontSize = 18;
											default_size.captionHeight = 250;
											default_size.captionWidth = 250;
											default_size.padding = 35;

										} else {

											default_size.fontSize = 12;
											default_size.captionHeight = 70;
											default_size.captionWidth = 200;
											default_size.padding = 12;
											default_size.zoom_toolbox = '70%';
											default_size.rule_readable = '0px 1px 0px #ccc';
										}

										// /////////////////////////////////////////////////
										//handling event

										$modal_list.on('mouseenter',
											function(e) {
												$modal_list.css({
													opacity : 1
												});
												$modal_list.find('.tree').animate(
												{
													'margin-left' : "+="+ ($modal_list.width()+30),
												},
												1000);
															// e.stopPropagation();

														});
										$modal_list.on('mouseleave',function(e) {
											$modal_list.css({
												opacity : 1
											});
											$modal_list.find('.tree').stop(true,true).animate(
											{
												'margin-left' : "-="+ ($modal_list.width()),
											},1000);
															// e.stopPropagation();

														});
										$trigger_area.on('mouseleave',function(e) {
											$modal_list.css({
												opacity : 1
											});
											$modal_list.find('.tree').stop(true,true).animate(
											{
												'margin-left' : "-="+ ($modal_list.width()),
											},1000);
											e.stopPropagation();

										});
										$trigger_area.on('mouseenter',function(e) {

											$modal_list.find('.tree').stop(true,true).animate(
											{
												'margin-left' : "+="+ ($modal_list.width() + 20),
											},2000);
											e.stopPropagation();

										});
										$('.tree').children().on('click wordontop.update',function(e) {
											e.stopPropagation();
										});
										
										$global.on('click wordontop.update',function(e,coord_update) {
											if (e.which == 3) {
												e.preventDefault();
												return false;
											}
											var self = this;
															// ///////////////////////////////////////////////
															// handling if what I get from server is correct
															var msg_bubble_info = "";
															switch (settings.load) {
																case LOADING_EMPTY:
																msg_bubble_info = g
																._("commentez cette nouvelle image");
																break;
																case LOADING_ERROR:
																msg_bubble_info = g._("problème lors du chargement");
																settings.error += msg_bubble_info;
																break;
															}
															var $bubble_info = $("<div class='bubble'>"+ msg_bubble_info+ "</div>");
															$bubble_info.css({
																left : settings.$new.parent().position().left+ parseInt(settings.width / 2),
																top : settings.$new.parent().position().top + parseInt(settings.height / 2),
																'z-index' : 99999
															});
															$global.append($bubble_info);

															console.log("wordontop : "+ g._('panel du plugin cliqué'));
															var x, y, color, font, nbcaption, content, size, style;
															$(this).css('height',settings.height);

															if (!coord_update) {
																style = '';
																x = e.pageX- this.offsetLeft;
																y = e.pageY- this.offsetTop;
																color = "#cc3333";
																font = "Impact,Charcoal,sans-serif";
																nbcaption = settings.nb_caption++;
																content = "";
																size = default_size.fontSize;

																console.log("wordontop : "+ e.target.nodeName);
																$global = $(e.target).parent();
																console.log($(e.target));
																console.log("wordontop : "+ e.target.nodeName);
																$global = $(e.target).parent();
																console.log($(e.target));

															} else {
																if (!testJSON(coord_update)) {
																	// fail
																	return -1;
																}
																style = coord_update.style|| '';
																x = coord_update.x ? parseInt(coord_update.x): 0;
																y = coord_update.y ? parseInt(coord_update.y): 0;
																color = coord_update.color ? coord_update.color: "#cc3333";
																font = coord_update['font-family'] ? coord_update['font-family']: "Impact,Charcoal,sans-serif";
																nbcaption = coord_update.nbcaption;
																content = coord_update.content;
																size = coord_update['font-size'] ? coord_update['font-size']: default_size.fontSize;

															}

															settings.$global.addClass('focus');
															var $input = $("<textarea class='' id='caption"+ nbcaption+ "'  >"+ content+ "</textarea>");
															var $font_chooser = $('<input class=\"font\" value=\"'+ font+ '\" />');
															var chain = "";
															for ( var i = 8; i < 64; i += 2) {
																var selected = "";
																if (i == size) {
																	selected = 'selected';
																}
																chain += '<option '+ selected+ ' value='+ i+ '>'+ i+ 'px</option>';
															}
															var $size_chooser = $("<select class='size_chooser dd-selected'>"+ chain+ "</select>");

															$bloc_input = $("<div class='caption "+ settings.theme+ " "+ (coord_update ? "animation-arrival"+ Math.floor(Math.random() * 4 + 1): "")+ "' id='bloc_caption"+ nbcaption+ "'>")
															.append($input);
															$input.wrap("<div class='input'/>");
															$toolbox = $("<div class='toolbox' />");
															$submit_caption = $("<div  class='submit_caption'><div class='submit_wrapper'> "+ g._('envoyer')+ "<span class='icon' style='font-family : RaphaelIcons;'> Ã</span></div></div>");
															$toolbox.css('zoom',default_size.zoom_toolbox);
															var id = $bloc_input.attr('id')+ '';
															settings.captions[id] = new captions();
															var $close = $("<div class='close' />");
															var $style_wrapper = $("<div class='style_wrapper' />");
															var $color_chooser = $("<input class=\"simple_color\" value=\""+ color+ "\">");
															var $3d_chooser = $("<span class='desc_3d'>3D</span><input name='mode_3d' value='active' type='checkbox'/>");
															var $underline_chooser = $("<span class='desc_underline'>a</span><input name='mode_underline' value='active' type='checkbox'/>");
															var $bold_chooser = $("<bold class='desc_bold'>A</bold><input name='mode_bold' value='active' type='checkbox'/>");

															function get_random_color() {

																var t = hslToRgb(Math.random(),Math.random(),0.2);
																var color = 'rgba('+ Math.floor(t[0])+ ','+ Math.floor(t[1])+ ','+ Math.floor(t[2])+ ','+ 0.01+ ')';

																console.log("wordontop : "+ g._('couleur')+ " :");
																console.log("wordontop : "+ color);
																return color;
															}

															$input.css('text-shadow',default_size.rule_readable);

															var color_tmp = get_random_color();
															$bloc_input.css(
																'background-color',
																color_tmp);

															var $true_font_chooser;
															var caption_layer = "";
															$input.on('active',
																function() {
																			// $(this).parent().parent().trigger('mouseenter');
																		});
			
															$bloc_input.mouseenter(function(e) {
																$toolbox.show();
																if ($bloc_input.css('z-index') != '800')
																	$(e.target).css('z-index','800');
																$font_chooser.on('change',
																	function() {
																		hint_to_post();
																		$bloc_input.trigger('change_width');
																		console.log("wordontop : "+ g._('police changée'));
																		$input.css(
																			'font-family',
																			$(this).val());
																		// settings.fontArr[$(this).parent().attr('id')] = $(this).val();
																		changeCaption(settings,$(this).parent().attr('id'),'font-family',$(this).val());
																		// $input.trigger('change');
																		return false;
																	});

																$font_chooser.before("<span class='font_icon' style='display :inline-block;position : relative;margin-left : -25px;margin-top : -25px;>Tt</span>");
																$font_chooser.show();
																$input.trigger('update');
																$(this).trigger('click');
																color_tmp = $(this).css('background-color');
																var color_rgb = color_tmp.split(',');
																var color_rgb = color_rgb[0]+ ','+ color_rgb[1]+ ','+ color_rgb[2]+ ',0.01)';
															console.log("wordontop : "+ color_rgb);
															$(this).css(
																'background-color',color_rgb);
															if (!settings.simple) {
																$toolbox.fadeIn();
															}
															return false;
															});
															$bloc_input.mouseleave(function(e) {
																$('.font_icon').remove();
																$(e.target).css('z-index',78);
																$(this).css('background-color',color_tmp);
																//$toolbox.fadeOut();
																$(this).find('textarea').blur();
															});
															$toolbox_bloc1 = $("<div class='toolbox_bloc1'/>");
															$toolbox_bloc1.css({
																display : 'inline-block',
																'margin-left' : '28px',
															});
															$toolbox_bloc1.append($style_wrapper);
															$style_wrapper.append($3d_chooser);
															$style_wrapper.append($underline_chooser);
															$style_wrapper.append($bold_chooser);
															$toolbox_bloc1.append($font_chooser);
															$bloc_input.append($toolbox);
															$bloc_input.append($close);

															var self = this;
															this.$input = $input;
															this.settings = settings;
															this.$size_chooser = $size_chooser;
															this.$bloc_input = $bloc_input;
															$toolbox_bloc1.append($size_chooser);


															$toolbox_bloc1.append($color_chooser);
															$color_chooser.simpleColor({
																'buttonClass' : 'hidden'
															});
															$input.parent().height($input.outerHeight());
															$input.parent().width($input.outerWidth());
															update_style = function() {
																// 3D
																if ((style & 4) == 4) {
																	$3d_chooser.attr('checked',true);

																	$3d_chooser.trigger('change_3d');
																}
																// underline
																if ((style & 2) == 2) {
																	$underline_chooser.attr('checked',true);
																	$underline_chooser.trigger('change_underline');
																}
																if ((style & 1) == 1) {
																	$bold_chooser.attr('checked',true);
																	$bold_chooser.trigger('change_bold');
																}
															};
															hint_to_post = function(
																isOutlined) {
																if (typeof isOutlined == 'undefined') {
																	isOutlined = false;
																}

																if (!settings.simple) {
																	if (!isOutlined)
																		$input.parent().css(
																		{
																			'-webkit-box-shadow' : ' 0  0 5px rgba(128, 0, 0, 1)',
																			'-moz-box-shadow' : ' 0 0 5px rgba(128, 0, 0, 1)',
																			'box-shadow' : '0  0 5px rgba(128, 0, 0,1)',
																		});
																	$input.parent().height($input.outerHeight());
																	$input.parent().width($input.outerWidth());
																	$bloc_input.trigger('resize');
																}
															};
															$toolbox.css('width','100%');
															$toolbox.append($toolbox_bloc1);
															$input.focus(function() {
																hint_to_post();
																$bloc_input.mouseenter();
															});
															//important : initiali plugins after inserting in the DOM
															$true_font_chooser = $font_chooser.fontSelector();
															var str_color_size_chooser ='';
															if(settings.theme === '')str_color_size_chooser = $bloc_input.css('background-color');
															$size_chooser.ddslick({
																onSelected : function(data) {
																	$input.trigger('change_size');
																	console.log("wordontop : "+ g._('changement de taille du texte'));
																	console.log("wordontop : "+ data.selectedData.text);
																	$input.css('font-size',data.selectedData.text);
																	changeCaption(settings,$bloc_input.attr('id'),'font-size',$size_chooser.val());
																	return false;
																},
																background : str_color_size_chooser,
															});
															if (coord_update) {
																changeCaption(settings,$bloc_input.attr('id'),'font-family',coord_update['font-family']);
																changeCaption(settings,$bloc_input.attr('id'),'font-size',coord_update['font-size']);
																changeCaption(settings,$bloc_input.attr('id'),'content',content);

																if ('_id' in coord_update)
																	changeCaption(settings,$bloc_input.attr('id'),'_id',coord_update['_id']);
															}
															changeCaption(settings,$bloc_input.attr('id'),'style',style);
															changeCaption(settings,$bloc_input.attr('id'),'src',settings.src);

															if(isNaN(x)){
																x=y=0;
															}
															changeCaption(settings,$bloc_input.attr('id'),'x', x);
															changeCaption(settings,$bloc_input.attr('id'),'y', y);

															console.log("wordontop : "+ g._("coordonnées des légendes : ")+ settings.captions[$bloc_input.attr('id')+ ''].y);
															changeCaption(settings,$bloc_input.attr('id'),'nbcaption',nbcaption);

															$input.css({
																'color' : color,
																'font-size' : size,
																'font-familly' : font,
															});

															// ///////////////////////////////////////////////////////////////handling events
															$input.click(function(){
																$(this).focus();
															});
															$submit_caption.on("click",function() {
																if (settings.callback) {
																	settings.callback(new Error(settings.error),JSONify(settings));
																}
																if(settings.post != ""){
																	for (oo in settings.captions) {
																		oo.change = false;
																	}
																	changeCaption(settings,$(this).parent().parent().attr('id'),'content',$input.val());
																	$bloc_input.children('.input').css('box-shadow','none');
																	methods.POST(settings.server_location,settings.captions[$(this).parent().parent().attr('id')]);
																}
															});
															$bold_chooser.on('click change_bold',function(e) {

																if ($(this).is(':checked')) {
																	style |= 1;
																	changeCaption(settings,$bloc_input.attr('id'),'style',style);
																	$input.css('font-weight','900');
																} else {
																	$input.css(
																		'font-weight',
																		'600');
																}
																e.stopPropagation();
															});
															$underline_chooser.on('click change_underline',
																function(e) {
																	if ($(this).is(':checked')) {
																		style |= 2;
																		changeCaption(settings,$bloc_input.attr('id'),'style',style);
																		$input.css('text-decoration','underline');
																	} else {
																		$input.css('text-decoration','none');
																	}
																	e.stopPropagation();
																});
															var color_post_3d = '';
															$3d_chooser.on('click change_3d',function(e) {
																if ($(this).is(':checked')) {
																	style |= 4;
																	changeCaption(settings,$bloc_input.attr('id'),'style',style);

																					// check size of the font to enhance the effect
																	var tab_distance_blur = [ 0 ];
																	if ($size_chooser.val() >= 22) {
																		tab_distance_blur = [2,4,6,8,3,8,15,3,8,15];

																	}
																	if ($size_chooser.val() < 22) {
																		tab_distance_blur = [1,3,5,6,1,7,12,2,7,13];
																	}
																	// initialize process to compute the color to apply
																	var color = color_post_3d = $input.css('color');

																	var str_effect = "\
																	0 "+ parseInt(tab_distance_blur[0])+ "px 0 "+ darken(color,14)+ ",\
																	0 "+ parseInt(tab_distance_blur[1])+ "px 0 "+ darken(color,16)+ ",\
																	0 "+ parseInt(tab_distance_blur[2])+ "px 0 "+ darken(color,18)+ ",\
																	0 "+ parseInt(tab_distance_blur[3])+ "px 0 "+ darken(color,20)+ ",\
																	"+ parseInt(tab_distance_blur[4])+ "px "+ parseInt(tab_distance_blur[5])+ "px "+ parseInt(tab_distance_blur[6])+ "px rgba(0,0,0,0.1),\
																	"+ parseInt(tab_distance_blur[7])+ "px "+ parseInt(tab_distance_blur[8])+ "px "+ parseInt(tab_distance_blur[9])+ "px rgba(0,0,0,0.3)";
																	
																	// apply effect with css3

																	$input.css({
																		'text-shadow' : str_effect,
																	});
																	$(this).on('select',function() {
																			$input.css('color',color_post_3d);
																		});
																}else{
																	$input.css({
																		'text-shadow' : 'none',
																	});
																}
																e.stopPropagation();

																});
																		$color_chooser.on('change',function() {
																				hint_to_post();
																				console.log("wordontop : "+ g._('couleur modifiée'));
																				$input.css('color',$(this).val());
																				$('.simpleColorDisplay:before').css(
																					'background-color',$(this).val());
																				changeCaption(settings,$bloc_input.attr('id'),'color',$(this).val());
																				update_style();
																				$bloc_input.trigger('click');
																			});

																		$bloc_input.on('click',function(eve) {
																				console.log("wordontop : "+ g._("légende cliquée"));
																				var y_in = 0;
																				var x_in = parseInt($input.width());
																				var height_children = 0;
																				var self = this;
																				if (!settings.simple) {
																					y_in = $toolbox.outerHeight();
																					y_in += $input.outerHeight();
																					y_in -= $(this).children('ul').outerHeight();
																				} else {
																					y_in += $input.outerHeight();
																					y_in -= $(this).children('ul').outerHeight();
																				}
																				$(this).css('height',y_in+ 'px');
																				$(this).width(x_in + 59);
																				return false;
																			});

																		$input.on('keyup change simulate_change',function(e) {
																				// hint_to_post(false);
																				
																				var o = $bloc_input.attr('id');
																				changeCaption(settings,$bloc_input.attr('id')+ '','change',true);
																				changeCaption(settings,$bloc_input.attr('id'),'content',$(this).val());
																			
																				var code = (e.keyCode ? e.keyCode : e.which);
																				if (code == 13) {
																					if (settings.callback) {
																						settings.callback(new Error(settings.error),JSONify(settings));
																					}
																					if (settings.post != "") {
																						for (oo in settings.captions) {
																							oo.change = false;
																						}
																						$bloc_input.children('.input').css('box-shadow','none');
																						var c;
																						c = settings.captions[o].content;
																						if (c.charAt(c.length - 1) == '\n') {
																							c = c.slice(0,-1);
																					}

																					settings.captions[o].content = c;
																					methods.POST(settings.server_location,settings.captions[o]);

																				}
																			} else {
																				hint_to_post();
																			}
																			e.stopPropagation();
																		});
																		$input.on('mousemove change_size keypress',function(eve) {
																				console.log("wordontop : "+ g._('redimensionnement du champs'));
																				$input.parent().height(
																				$input.outerHeight());
																				$input.parent().width(
																				$input.outerWidth());
																				$bloc_input.trigger('click');
																			});

																		$close.on('click',function(eve) {
																				$(this).parent().remove();
																				methods.DELETE(null,settings.captions[$(this).parent().attr('id')]);
																				delete settings.captions[$(this).parent().attr('id')];
																				return false;
																			});
																		darken = function(rgb, per) {

																			var color = rgb.split(',');
																			var color_rgb = [
																			parseInt(color[0].split('(')[1]),parseInt(color[1]),parseInt(color[2]) ];
																			// alert(color_rgb);
																			var hsl = rgbToHsl(color_rgb[0],color_rgb[1],color_rgb[2]);
																			var res = hslToRgb(hsl[0],hsl[1],hsl[2]- (hsl[2] * (per / 100)));
																			// alert(res);
																			return "rgb("+ Math.ceil(res[0])+ ","+ Math.ceil(res[1])+ ","+ Math.ceil(res[2])+ ")";

																		};

														$close.on('hover',function(e) {
																var content;
																var img_tmp = new Image();
																img_tmp.src = $(this).css('background-image').replace(/"/g,"").replace(/url\(|\)$/ig,"");
																content = img_tmp.contentDocument;
																console.log(content);
															//	fade(content);
															});
														$submit_caption.css({
															'display' : 'inline-block',
															'position' : 'relative',
															'float' : 'left',
															'vertical-align' : 'middle',
														});
														$toolbox.append($submit_caption);
														$global.append($bloc_input);
														changeCaption(settings,$bloc_input.attr('id')+ '','change',false);

														$bloc_input.css({
															'position' : 'absolute',
															'margin-left' : 0,
															'margin-top' : 0,
															'width' : default_size.captionWidth,
															'height' : default_size.captionHeight,
															'padding' : default_size.padding,
														});
														var isParentInline = false
														$(e.target).parents().each(function(){
															if($(this).css('display') == 'inline'){
																isParentInline = true;
															}
															});
														if ((!($(e.target).parents().filter('td').length > 0) && !($(e.target).parent().filter('p').length > 0) && !isParentInline)) {
															$bloc_input.css({
															'left' : x + settings.$new.parent().position().left,
															'top' : y + settings.$new.parent().position().top,
														});

														} else {
															$bloc_input.css({
																'left' : x,
																'top' : y,
															});

														}
														$input.focus();
													/*	$bloc_input.slideUp(20);
														$toolbox.slideUp(50);
														$bloc_input.stop(true,true).slideDown(500);*/
														
														if (settings.simple) {
															$bloc_input.css({
																'background' : 'red  -175px 0',
															});
															$input.parent().css(
															{
																'height' : '100%',
																'min-width' : '100%',
															});
															$input.height($input.parent().parent().outerHeight());
															$input.width($input.parent().parent().outerWidth());
															$('.toolbox').hide();
														} else {
															$input.autoGrowInput();
														}

														$input.parent().height($input.height());
														$input.parent().width($input.width());
														$input.trigger('update');

														this.settings = settings;
														var self = this;

														$bloc_input.trigger('click');
														$bloc_input.draggable({
															containment : [
															self.settings.$new.parent().position().left - (parseInt($("#bloc_caption0").length ? $("#bloc_caption0").css('padding-left')
															.slice(0,-2) : 0)),
															self.settings.$new.parent().position().top - (parseInt($("#bloc_caption0").length ? $("#bloc_caption0").css('padding-top')
															.slice(0,-2) : 0)),
															(self.settings.$new.parent().position().left + settings.width) - (parseInt($("#bloc_caption0").length ? $("#bloc_caption0")
															.css('padding-left').slice(0,-2): 0)),(self.settings.$new.parent().position().top + settings.height) - (parseInt($("#bloc_caption0").length ? $("#bloc_caption0")
															.css('padding-bottom')
															.slice(0,-2) : 0) + $("#bloc_caption0").find('.input').outerHeight()), ],
															stop : function(event,ui) {
																			// settings.positionArr[$(this).attr('id')
																			// +
																			// '']
																			// =
																			// $(this).position();
																			changeCaption(settings,$(this).attr('id')+ '','x',parseInt($(this).position().left- $(this).parent().position().left));
																			changeCaption(settings,$(this).attr('id')+ '','y',parseInt($(this).position().top- $(this).parent().position().top));
																			if (settings.post != "") {
																				for (oo in settings.captions) {
																					oo.change = false;
																				}
																				changeCaption(settings,$(this).parent().attr('id'),'content',$input.val());
																				console.log(settings.captions[$(this).attr('id')]);
																				$bloc_input.find('.input').css('box-shadow',0);
																				$(this).find('.input').css('box-shadow',0);
																				methods.POST(
																					settings.server_location,
																					settings.captions[$(this).attr('id')]);
																				changeCaption(settings,$(this).attr('id')+ '','change',false);
																			}
																			console.log("wordontop : "+ g._("position fin  de drag : "));
																			console.log($(this).position());
																		},
																	});

															// //////////////////////////////////////////////////////////////:
															// handling css

															//triggered by the plugin AutoGrowUp in this file
																$bloc_input.on('change_width',function() {
																	$toolbox.width($input.outerWidth());
																	$toolbox_bloc1.width(parseInt($input.outerWidth() / 3));
																	$submit_caption.height(parseInt($toolbox.outerHeight() / 2));
																	$submit_caption.css(
																		{
																			'padding-top' : ($submit_caption.height() / 2)- parseInt($submit_caption.css('fontSize'),10)
																		});
																	$submit_caption.width(parseInt($input.outerWidth() / 3));
																	var max = 0;
																	$toolbox_bloc1.children().each(
																		function() {
																			if ($(this).width() > max)
																				max = $(this).width();
																		});
																	$submit_caption.css({
																		'margin-top' : '5px',
																		'min-width' : '12%',
																						// 'margin-left':max,

																	});
																	$('.simpleColorSelectButton').css(
																		'display',
																		'none');
																	$('.simpleColorDisplay').on('click',function() {
																		$('.simpleColorCell').on('click',function() {
																			$('.hidden').css('display','none');
																		});
																	});
																});

																$bloc_input.trigger('change_width');
																$size_chooser.on('change click',function() {
																		$bloc_input.trigger('change_width');
																		hint_to_post();
																		$(this).addClass('active');
																		return false;
																	});
																$size_chooser.trigger('change');
																update_style();

																e.preventDefault();
																e.stopPropagation();
																});

																// correct size container after change
																// of the children

																// settings.$global.each(function() {
																// var newHeight = 0, newWidth = 0,
																// $this = $(this);
																// $.each($this.children(), function() {
																// newHeight += $(this).height();
																// newWidth += $(this).width();
																// });
																// $this.height(newHeight);
																// $this.width(newWidth);
																// });

																//methods.tayau = this;

																}
																jQuery.fn.methods = methods;
																setTimeout(function(){
																	WORDONTOP_FINISH_LOADING = true;

																},5000);
																return methods;

																},

																/**
																 * put text saying reason why
																 */
																 error = function(destination) {
																 	console.error(destination);

																 },
																 makeAbsolute = function(rebase) {

																 	var el = rebase.$global;
																 	var index = el.index();
																 	var $children = el.children().remove();
																 	var pos = el.position();

																 	el.css({
																 		position : "relative",
																		// top : pos.top,
																		// left : pos.left,
																		'z-index' : 1,
																		'max-height' : rebase.height,
																		'max-width' : rebase.width,
																		'margin-bottom' : '3px',
																		// 'margin-top' : rebase.margintop,
																		'margin-bottom' : rebase.marginbottom,
																		'margin-left' : rebase.marginleft,
																		'margin-right' : rebase.marginright,

																	});
																	if (rebase) {
																			var gg = el.parent();
																		if (gg.children().eq(index)[0] == gg
																			.children().eq('last')[0]) {
																			gg.children().eq(index - 1).after(
																				el);
																	} else {
																		gg.children().eq(index).before(el);
																	}
																		
																	}
																},
																getPosition = function(e) {
																	var left = 0;
																	var top = 0;
																	while (e.offsetParent) {
																		left += e.offsetLeft;
																		top += e.offsetTop;
																		e = e.offsetParent;
																	}
																	left += e.offsetLeft;
																	top += e.offsetTop;
																	return {
																		x : left,
																		y : top
																	};
																},
																initlib = function(name, settings, silent) {
																	excanvaslibPath = getRelativePath(
																		settings.server_location, name);
																	if (typeof silent == 'undefined')
																		silent = false;
																	var last = 0;
																	for (c in excanvaslibPath) {
																		last = c;
																	}
																	if (silent)
																		console.log("wordontop : "
																			+ excanvaslibPath);

																	jQuery.ajax({
																		async : false,
																		type : "GET",
																		url : excanvaslibPath,
																		data : null,
																		success : function(script,textStatus) {

																			if (!silent)
																				console.log("wordontop : "
																				+ textStatus
																				+ wordontop_g
																				._(" chargement de  : ")
																				+ name);
																			eval(script);
																		},
																		dataType : 'script'
																	});

																},
																getRelativePath = function(source, target) {
																	var sep = (source.indexOf("/") !== -1) ? "/": "\\", targetArr = target.split(sep);
																	var sourceArr = source.split(sep);
																	var osef = sourceArr.pop();
																	var pathComplete = ((source+ "/javascripts/Word_on_top" + "/").replace(new RegExp(",", "gi"), "/"))+ target;
																	return pathComplete;
																},
																redraw = function(settings) {

																	var $canvas = $("<canvas id='graph'></canvas>");
																	settings.$global.append($canvas);
																	console.log("wordontop : " + 'marginleft');
																	console.log("wordontop : "+ settings.marginleft);
																	settings.$canvas = $canvas.css('position', 'absolute').css('left',settings.$new.position().left+ (settings.$new.outerWidth() - settings.$new.width()))
																	.css('top',settings.$new.position().top+ (settings.$new.outerHeight() - settings.$new.height()))
																	.css('min-height',settings.height + "px")
																	.css('max-width',settings.width + "px")
																	.attr('width', settings.width)
																	.attr('height', settings.height);

																	if (jQuery.browser.msie) {

																		canvas = jQuery(".wordontopclass")[0];
																		G_vmlCanvasManager.initElement(canvas);
																	}

																	// set new size of the canvas.

																	jQuery(".wordontopclass").attr("max-height", settings.height)
																	.attr("max-width", settings.width);
																	settings.$new.css('position', '');

																},
																clone = function(obj) {
																	if (null == obj || "object" != typeof obj)
																		return obj;
																	var copy = obj.constructor();
																	for ( var attr in obj) {
																		if (obj.hasOwnProperty(attr))
																			copy[attr] = obj[attr];
																	}
																	return copy;
																},
																JSONify = function(settings) {

																	console.log(settings['captions']);
																	var res = {
																		title : "",
																		width : 0,
																		height : 0,
																		captions : {}
																	};
																	var i = 0;
																	for (c in settings.captions) {
																		res.captions[i] = settings.captions[c];
																		i++;
																	}
																	res.title = settings.src;
																	res.width = settings.width;
																	res.height = settings.height;
																	console.log("wordontop : "+ wordontop_g._('objet jsonifié : '));
																	console.log("wordontop : ", res);
																	return res;

																},
																getServerLocation = function(settings){
																	var division_relative = document.location.href.toString().split(':');
																	var domain_slash = division_relative[1];
																	var domain = domain_slash.split("/")[2];

																	var port = division_relative[2].split('/')[0];
																	var adress;
																	if(!isNaN(parseInt(port))){
																		adress = domain+':'+port;
																	}else{
																		adress = domain;
																	}
																	if(settings['server_location'] == ""){
																		alert(adress);
																		settings['server_location'] = adress;
																	}
																},
																testJSON = function(coord_update) {
																	var permit_index = {
																		'_id' : '',
																		'src' : '',
																		'change' : '',
																		'height' : '',
																		'width' : '',
																		'x' : '',
																		'y' : '',
																		'color' : '',
																		'font-family' : '',
																		'font-size' : '',
																		'nbcaption' : '',
																		'content' : '',
																		'style' : '',
																	};

																	for (j in coord_update) {

																		if (!permit_index.hasOwnProperty(j)) {
																			error(g
																				._("json incorrect!, indice : ")
																				+ j + g._(" inconnu ! "));
																			return false;
																		}
																	}
																	return true;

																}, changeCaption = function(set, id, param,
																	newValue) {

																	if (id) {

																		set.captions[id].change = true;
																		set.captions[id][param] = newValue;
																		$('#' + id).data(set.captions[id]);
																	}

																}, /**
																	 * Converts an RGB color value to HSL.
																	 * Conversion formula adapted from
																	 * http://en.wikipedia.org/wiki/HSL_color_space.
																	 * Assumes r, g, and b are contained in the
																	 * set [0, 255] and returns h, s, and l in
																	 * the set [0, 1].
																	 * 
																	 * @param Number
																	 *            r The red color value
																	 * @param Number
																	 *            g The green color value
																	 * @param Number
																	 *            b The blue color value
																	 * @return Array The HSL representation
																	 */
																	 rgbToHsl = function(r, g, b) {
																	 	r /= 255, g /= 255, b /= 255;
																	 	var max = Math.max(r, g, b), min = Math
																	 	.min(r, g, b);
																	 	var h, s, l = (max + min) / 2;

																	 	if (max == min) {
																	 		h = s = 0;
																		// achromatic
																	} else {
																		var d = max - min;
																		s = l > 0.5 ? d / (2 - max - min) : d
																		/ (max + min);
																		switch (max) {
																			case r:
																			h = (g - b) / d + (g < b ? 6 : 0);
																			break;
																			case g:
																			h = (b - r) / d + 2;
																			break;
																			case b:
																			h = (r - g) / d + 4;
																			break;
																		}
																		h /= 6;
																	}

																	return [ h, s, l ];
																},

																/**
																 * Converts an HSL color value to RGB.
																 * Conversion formula adapted from
																 * http://en.wikipedia.org/wiki/HSL_color_space.
																 * Assumes h, s, and l are contained in the set
																 * [0, 1] and returns r, g, and b in the set [0,
																 * 255].
																 * 
																 * @param Number
																 *            h The hue
																 * @param Number
																 *            s The saturation
																 * @param Number
																 *            l The lightness
																 * @return Array The RGB representation
																 */
																 hslToRgb = function hslToRgb(h, s, l) {
																 	var r, g, b;

																 	if (s == 0) {
																 		r = g = b = l;
																		// achromatic
																	} else {
																		function hue2rgb(p, q, t) {
																			if (t < 0)
																				t += 1;
																			if (t > 1)
																				t -= 1;
																			if (t < 1 / 6)
																				return p + (q - p) * 6 * t;
																			if (t < 1 / 2)
																				return q;
																			if (t < 2 / 3)
																				return p + (q - p)
																			* (2 / 3 - t) * 6;
																			return p;
																		}

																		var q = l < 0.5 ? l * (1 + s) : l + s
																		- l * s;
																		var p = 2 * l - q;
																		r = hue2rgb(p, q, h + 1 / 3);
																		g = hue2rgb(p, q, h);
																		b = hue2rgb(p, q, h - 1 / 3);
																	}

																	return [ r * 255, g * 255, b * 255 ];
																}
															})(jQuery);
														});