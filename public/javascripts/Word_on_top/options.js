window.bookmarklet.options = {
	css : "http://localhost:4000/stylesheets/Word_on_top/WordOnTop.css",
	js : [
	'http://localhost:4000/javascripts/jquery-1.7.2.min.js',
	'http://localhost:4000/javascripts/Word_on_top/jquery-ui-1.8.20.custom.min.js',
	'http://localhost:4000/javascripts/Word_on_top/jquery.simple-color.min.js',
	'http://localhost:4000/javascripts/Word_on_top/jquery.ddslick.min.js',
	'http://localhost:4000/javascripts/Word_on_top/pixel_effect.js',
	'http://localhost:4000/javascripts/Word_on_top/wordOnTop.js',
	'http://localhost:4000/javascripts/wot.js',
	],

	ready: function() {
		var a = $("<div style='-webkit-border-radius: 3px;border-radius: 3px;background-color: #878787;padding : 60px;color: black;position : fixed;z-index : 999;'class='message_plugin'>plugin Word On Top chargé</div>");
		
		var viewportWidth = ( (document.documentElement.clientWidth || (document.body.clientWidth || 0)));
		var viewportHeight = ( (document.documentElement.clientHeight || (document.body.clientHeight || 0)));
	

		$('body').append(a);
		 var element_height = a.outerWidth()/2;
		 var element_width = a.outerHeight()/2;
		a.hide();
		a[0].style.left =( viewportWidth/2 -element_height) + 'px';
		a[0].style.top =(viewportHeight/2 -element_width )+ 'px';
		a.fadeIn().delay(2000).fadeOut();
	}
};

